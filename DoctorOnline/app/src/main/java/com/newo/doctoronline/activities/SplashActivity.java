package com.newo.doctoronline.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import com.newo.doctoronline.R;
import com.newo.doctoronline.constants.Constants;
import com.newo.doctoronline.utils.SharedPreference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Log.w(TAG, "onCreate()");

        //Cal setupView method
        setupView();
    }

    /**
     * Setup View components
     */
    private void setupView() {
        SharedPreference.getInstance(this);
        //Install custom fonts via Calligraphy library
//        printHashKey();
        setupUiInteractions();
    }

    /**
     * Setup Ui interactions method
     */
    private void setupUiInteractions() {
        //On click methods

        //Call setup splash handler method
        splashHandler();
    }


    /**
     * Setup splash handler
     */
    private void splashHandler() {
        new Handler().postDelayed(new Runnable() {
            // Showing splash screen with a timer.
            @Override
            public void run() {
                // This method will be executed once the timer is over
                int state = SharedPreference.getInt(Constants.APP_STATE);
                if (state == Constants.APP_STATE_DOWNLOADED)
                    openIntro();
                else if (state == Constants.APP_STATE_LOGGED_OUT)
                    openLoginOptions();
                else if (state == Constants.APP_STATE_LOGGED_IN)
                    finish();
            }
        }, Constants.SPLASH_TIME_OUT);
    }

    private void openIntro(){
        Intent intentLogin = new Intent(this, IntroActivity.class);
        startActivity(intentLogin);
        finish();
    }

    private void openLoginOptions(){
        Intent intentLogin = new Intent(this, LoginOptionsActivity.class);
        startActivity(intentLogin);
        finish();
    }

    private void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.peership", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.v("TEMPTAGHASH KEY:",
                        "==>**********************"
                                + Base64.encodeToString(md.digest(),
                                Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();

        }
    }
}
