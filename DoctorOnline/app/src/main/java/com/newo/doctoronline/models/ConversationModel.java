package com.newo.doctoronline.models;

import java.util.HashMap;

/**
 * Created by elvinakhundzadeh on 7/2/17.
 */

public class ConversationModel {

    private String partnerName;
    private String partnerId;
    private String lastMessage;
    private HashMap<String, Object> lastMessageDate;
    private String groupId;
    private String photo;
    private String unreadMessagesCount;

    public ConversationModel(String partnerName, String partnerId, String lastMessage, HashMap<String, Object> lastMessageDate, String unreadMessagesCount) {
        this.partnerName = partnerName;
        this.partnerId = partnerId;
        this.lastMessage = lastMessage;
        this.lastMessageDate = lastMessageDate;
        this.unreadMessagesCount = unreadMessagesCount;
    }

    public ConversationModel() {
    }


    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public HashMap<String, Object> getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(HashMap<String, Object> lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public String getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public void setUnreadMessagesCount(String unreadMessagesCount) {
        this.unreadMessagesCount = unreadMessagesCount;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "ConversationModel{" +
                "partnerName='" + partnerName + '\'' +
                ", partnerId='" + partnerId + '\'' +
                ", lastMessage='" + lastMessage + '\'' +
                ", lastMessageDate=" + lastMessageDate +
                ", groupId='" + groupId + '\'' +
                ", photo='" + photo + '\'' +
                ", unreadMessagesCount='" + unreadMessagesCount + '\'' +
                '}';
    }
}
