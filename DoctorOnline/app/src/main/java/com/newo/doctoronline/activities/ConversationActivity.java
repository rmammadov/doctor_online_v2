package com.newo.doctoronline.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.newo.doctoronline.BuildConfig;
import com.newo.doctoronline.R;
import com.newo.doctoronline.adapters.ClickListenerChatFirebase;
import com.newo.doctoronline.models.ChatGroup;
import com.newo.doctoronline.models.ChatModel;
import com.newo.doctoronline.models.FileModel;
import com.newo.doctoronline.models.MapModel;
import com.newo.doctoronline.models.TestModel;
import com.newo.doctoronline.models.UserChat;
import com.newo.doctoronline.models.UserModel;
import com.newo.doctoronline.utils.CircleImageUtil;
import com.newo.doctoronline.utils.Utils;

import java.io.File;
import java.util.Date;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

import static com.newo.doctoronline.R.id.buttonMessage;
import static com.newo.doctoronline.R.id.sendLocation;
import static com.newo.doctoronline.R.id.sendPhoto;
import static com.newo.doctoronline.R.id.sendPhotoGallery;
import static com.newo.doctoronline.utils.FirebaseUtil.CHAT_GROUP_REFERENCE;
import static com.newo.doctoronline.utils.FirebaseUtil.CHAT_REFERENCE;
import static com.newo.doctoronline.utils.FirebaseUtil.FOLDER_STORAGE_IMG;
import static com.newo.doctoronline.utils.FirebaseUtil.URL_STORAGE_REFERENCE;
import static com.newo.doctoronline.utils.FirebaseUtil.USER_CHAT_REFERENCE;
import static com.newo.doctoronline.utils.FirebaseUtil.USER_MODEL_REFERENCE;
import static com.newo.doctoronline.utils.FirebaseUtil.addChatGroupFirebase;
import static com.newo.doctoronline.utils.FirebaseUtil.addChatModelFirebase;
import static com.newo.doctoronline.utils.FirebaseUtil.addMessageFirebase;
import static com.newo.doctoronline.utils.FirebaseUtil.addUserChatFirebase;
import static com.newo.doctoronline.utils.FirebaseUtil.updateChatGroupLastMessageFirebase;

public class ConversationActivity extends AppCompatActivity implements View.OnClickListener, ClickListenerChatFirebase {

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final int PLACE_PICKER_REQUEST = 3;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int RIGHT_MSG = 0;
    private static final int LEFT_MSG = 1;
    private static final int RIGHT_MSG_IMG = 2;
    private static final int LEFT_MSG_IMG = 3;

    private boolean chatTest = false;

    private final String TAG = "ConversationActivity";
    private String groupId;
    private String partner;
    private String message;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private DatabaseReference mFirebaseDatabaseReference;

    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private ClickListenerChatFirebase mClickListenerChatFirebase;
    private FirebaseRecyclerAdapter<ChatModel, ConversationActivity.MyChatViewHolder> mChatFirebaseAdapter;

    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView rvListMessage;
    private ImageView btSendMessage, btEmoji;
    private EmojiconEditText edMessage;
    private View contentRoot;
    private EmojIconActions emojIcon;

    private File filePathImageCamera;
    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        Intent intent = getIntent();

        if (intent.hasExtra("partnerId")) {
            partner = getIntent().getStringExtra("partnerId");
            Log.d("partnerId exists: " , partner);
        }

        if(getIntent().getStringExtra("groupId") != null)
            groupId = getIntent().getStringExtra("groupId");

        if (!Utils.checkInternetConnection(ConversationActivity.this)) {
            Utils.initToast(ConversationActivity.this, "Check your internet connection");
            finish();
        } else {
            setupView();
            verificaUsuarioLogado();
        }

        mClickListenerChatFirebase = this;
    }

    @Override
    public void clickImageChat(View view, int position, String nameUser, String urlPhotoUser, String urlPhotoClick) {

        Intent intent = new Intent(ConversationActivity.this, FullScreenImageActivity.class);
        intent.putExtra("nameUser",nameUser);
        intent.putExtra("urlPhotoUser",urlPhotoUser);
        intent.putExtra("urlPhotoClick",urlPhotoClick);
        startActivity(intent);
    }

    @Override
    public void clickImageMapChat(View view, int position, String latitude, String longitude) {
        String uri = String.format("geo:%s,%s?z=17&q=%s,%s", latitude,longitude,latitude,longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case buttonMessage:
                message = edMessage.getText().toString();
                sendMessageFirebase();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case sendPhoto:
                verifyStoragePermissions();
                break;
            case sendPhotoGallery:
                photoGalleryIntent();
                break;
            case sendLocation:
                locationPlacesIntent();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        StorageReference storageRef = storage.getReferenceFromUrl(URL_STORAGE_REFERENCE).child(FOLDER_STORAGE_IMG);

        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                System.out.println("Gallery request OK!");
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    sendFileFirebase(storageRef, selectedImageUri);
                } else {
                    //URI IS NULL
                }
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                System.out.println("Before all...");
                if (filePathImageCamera != null && filePathImageCamera.exists()) {
                    StorageReference imageCameraRef = storageRef.child(filePathImageCamera.getName() + "_camera");

                    System.out.println("Before firebase...");
                    sendFileFirebase(imageCameraRef, filePathImageCamera);
                } else {
                    //IS NULL
                }
            }
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {

                if (!chatTest) {

                    System.out.println("They have not talked before...");
                    groupId = mFirebaseDatabaseReference.push().getKey();

                    ChatGroup chatGroup = new ChatGroup(groupId, edMessage.getText().toString());
                    mFirebaseDatabaseReference.child(CHAT_GROUP_REFERENCE).push().setValue(chatGroup);

                    UserChat userChat = new UserChat(userModel.getId(), groupId, getIntent().getStringExtra("partner"), getIntent().getStringExtra("partnerId"), getIntent().getStringExtra("partnerId"));
                    UserChat userChatPartner = new UserChat(getIntent().getStringExtra("partnerId"), groupId, userModel.getName(), userModel.getId(), userModel.getId());

                    mFirebaseDatabaseReference.child(USER_CHAT_REFERENCE).push().setValue(userChat);
                    mFirebaseDatabaseReference.child(USER_CHAT_REFERENCE).push().setValue(userChatPartner);

                    Query addToCurrentUser = mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(userModel.getId());
                    addToCurrentUser.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            TestModel testModel = new TestModel();
                            testModel.setPartnerId(partner);
                            testModel.setGroupId(groupId);

                            for (DataSnapshot dS: dataSnapshot.getChildren()){
                                mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).child("testModel").push().setValue(testModel);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    Query addToPartner = mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(partner);
                    addToPartner.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            TestModel testModelPartner = new TestModel();
                            testModelPartner.setPartnerId(userModel.getId());
                            testModelPartner.setGroupId(groupId);


                            for (DataSnapshot dS: dataSnapshot.getChildren()){
                                mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).child("testModel").push().setValue(testModelPartner);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else{
                    updateChatGroupLastMessageFirebase("Location", groupId);
                }

                Place place = PlacePicker.getPlace(this, data);
                if (place != null) {

                    LatLng latLng = place.getLatLng();

                    MapModel mapModel = new MapModel(latLng.latitude + "", latLng.longitude + "");

                    ChatModel chatModel = new ChatModel(userModel, mapModel, userModel.getId(), groupId);
                    chatModel.setReceiverId(partner);
                    addChatModelFirebase(chatModel);
                } else {
                    //PLACE IS NULL
                }

                chatTest = true;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    photoCameraIntent();
                }
                break;
        }
    }

    private void setupView() {
        contentRoot = findViewById(R.id.contentRoot);
        edMessage = (EmojiconEditText) findViewById(R.id.editTextMessage);
        btSendMessage = (ImageView) findViewById(buttonMessage);
        btSendMessage.setOnClickListener(this);
        btEmoji = (ImageView) findViewById(R.id.buttonEmoji);
        emojIcon = new EmojIconActions(this, contentRoot, edMessage, btEmoji);
        emojIcon.ShowEmojIcon();
        rvListMessage = (RecyclerView) findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        mClickListenerChatFirebase = this;

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_activity_home);
        setSupportActionBar(myToolbar);

        partner = getIntent().getStringExtra("partnerId");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void sendMessageFirebase() {

        if (!chatTest) {

            chatTest = true;

            groupId = mFirebaseDatabaseReference.push().getKey();

            ChatGroup chatGroup = new ChatGroup(groupId, edMessage.getText().toString());
            UserChat userChat = new UserChat(userModel.getId(), groupId, getIntent().getStringExtra("partner"), getIntent(). getStringExtra("partnerId"), getIntent(). getStringExtra("partnerId") );
            UserChat userChatPartner = new UserChat(getIntent().getStringExtra("partnerId"), groupId, userModel.getName(), userModel.getId(), userModel.getId());

            addChatGroupFirebase(chatGroup);
            addUserChatFirebase(userChat);
            addUserChatFirebase(userChatPartner);

            Query addToCurrentUser = mFirebaseDatabaseReference.child("userModel").orderByChild("userId").equalTo(userModel.getId());
            addToCurrentUser.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    TestModel testModel = new TestModel();
                    testModel.setPartnerId(partner);
                    testModel.setGroupId(groupId);

                    for (DataSnapshot dS: dataSnapshot.getChildren()){
                        mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).child("testModel").push().setValue(testModel);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            Query addToPartner = mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(partner);
            addToPartner.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    TestModel testModelPartner = new TestModel();
                    testModelPartner.setPartnerId(userModel.getId());
                    testModelPartner.setGroupId(groupId);

                    for (DataSnapshot dS: dataSnapshot.getChildren()){
                        mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).child("testModel").push().setValue(testModelPartner);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            setAdapter();
        }else{
            updateChatGroupLastMessageFirebase(message, groupId);
        }

        addMessageFirebase(groupId, edMessage.getText().toString(), userModel, partner);

        chatTest = true;

        edMessage.setText(null);
    }

    private void retrieveMessages() {

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        Query query2 = mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(userModel.getId());

        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

               outer: for (DataSnapshot dS: dataSnapshot.getChildren()) {
                    for(DataSnapshot dSnapshot: dS.child("testModel").getChildren()){

                        if(dSnapshot.child("partnerId").getValue().equals(partner)){

                            chatTest = true;
                            groupId = dSnapshot.child("groupId").getValue().toString();

                            break outer;
                        }
                    }
                }
                setAdapter();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setAdapter(){

        mChatFirebaseAdapter = new FirebaseRecyclerAdapter<ChatModel, MyChatViewHolder>(
                ChatModel.class,
                R.layout.item_message_left,
                MyChatViewHolder.class,
                mFirebaseDatabaseReference.child(CHAT_REFERENCE).orderByChild("groupId").equalTo(groupId)

        ){
            @Override
            public MyChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view;

                if (viewType == RIGHT_MSG){
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right,parent,false);
                    return new MyChatViewHolder(view);
                }else if (viewType == LEFT_MSG){
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left,parent,false);
                    return new MyChatViewHolder(view);
                }else if (viewType == RIGHT_MSG_IMG){
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right_img,parent,false);
                    return new MyChatViewHolder(view);
                }else{
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left_img,parent,false);
                    return new MyChatViewHolder(view);
                }
            }

            @Override
            protected void populateViewHolder(MyChatViewHolder viewHolder, ChatModel model, int position) {

               // viewHolder.setIvUser(model.getUserModel().getPhoto_profile());
                viewHolder.setIvUser(model.getSenderId());
                viewHolder.setTxtMessage(model.getMessage());
                viewHolder.setTvTimestamp(model.getTimeStamp().get("date").toString());
                viewHolder.tvIsLocation(View.GONE);
                if (model.getFile() != null){
                    viewHolder.tvIsLocation(View.GONE);
                    viewHolder.setIvChatPhoto(model.getFile().getUrl_file());
                }else if(model.getMapModel() != null){
                    viewHolder.setIvChatPhoto(Utils.local(model.getMapModel().getLatitude(),model.getMapModel().getLongitude()));
                    viewHolder.tvIsLocation(View.VISIBLE);
                }
            }

            @Override
            public int getItemViewType(int position) {

                ChatModel model = getItem(position);

                if (model.getMapModel() != null){
                    if (model.getSenderId().equals(userModel.getId())){
                        return RIGHT_MSG_IMG;
                    }else{
                        return LEFT_MSG_IMG;
                    }
                }else if (model.getFile() != null){
                    if (model.getFile().getType().equals("img") && model.getSenderId().equals(userModel.getId())){
                        return RIGHT_MSG_IMG;
                    }else{
                        return LEFT_MSG_IMG;
                    }
                }else if (model.getSenderId().equals(userModel.getId())){
                    return RIGHT_MSG;
                }else{
                    return LEFT_MSG;
                }
            }
        };

        finishChat();
    }

    private void finishChat(){
        mChatFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mChatFirebaseAdapter.getItemCount();

                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();

                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    rvListMessage.scrollToPosition(positionStart);
                }
            }
        });
        rvListMessage.setLayoutManager(mLinearLayoutManager);

        rvListMessage.setAdapter(mChatFirebaseAdapter);
    }

    private void verificaUsuarioLogado() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            finish();
        } else {
            userModel = new UserModel(mFirebaseUser.getDisplayName(), mFirebaseUser.getUid());
            retrieveMessages();
        }
    }

    private void sendFileFirebase(StorageReference storageReference, final Uri file) {
        if (storageReference != null) {

            if (!chatTest) {

                groupId = mFirebaseDatabaseReference.push().getKey();

                ChatGroup chatGroup = new ChatGroup(groupId, edMessage.getText().toString());
                mFirebaseDatabaseReference.child(CHAT_GROUP_REFERENCE).push().setValue(chatGroup);

                UserChat userChat = new UserChat(userModel.getId(), groupId, getIntent().getStringExtra("partner"), getIntent().getStringExtra("partnerId"), getIntent().getStringExtra("partnerId"));
                UserChat userChatPartner = new UserChat(getIntent().getStringExtra("partnerId"), groupId, userModel.getName(), userModel.getId(), userModel.getId());

                mFirebaseDatabaseReference.child(USER_CHAT_REFERENCE).push().setValue(userChat);
                mFirebaseDatabaseReference.child(USER_CHAT_REFERENCE).push().setValue(userChatPartner);


                Query addToCurrentUser = mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(userModel.getId());
                addToCurrentUser.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        TestModel testModel = new TestModel();
                        testModel.setPartnerId(partner);
                        testModel.setGroupId(groupId);

                        for (DataSnapshot dS: dataSnapshot.getChildren()){
                            mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).child("testModel").push().setValue(testModel);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                Query addToPartner = mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(partner);
                addToPartner.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        TestModel testModelPartner = new TestModel();
                        testModelPartner.setPartnerId(userModel.getId());
                        testModelPartner.setGroupId(groupId);

                        for (DataSnapshot dS: dataSnapshot.getChildren()){
                            mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).child("testModel").push().setValue(testModelPartner);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }else
                updateChatGroupLastMessageFirebase("Image", groupId);

            final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
            StorageReference imageGalleryRef = storageReference.child(name + "_gallery");
            UploadTask uploadTask = imageGalleryRef.putFile(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
                    @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    FileModel fileModel = new FileModel("img", downloadUrl.toString(), name, "");
                    message = downloadUrl.toString();
                    ChatModel chatModel = new ChatModel(userModel, "", fileModel, userModel.getId(), groupId);
                    chatModel.setReceiverId(partner);

                    addChatModelFirebase(chatModel);
                }
            });

        } else {
            //IS NULL
        }
        chatTest = true;
    }

    private void sendFileFirebase(StorageReference storageReference, final File file) {
        if (storageReference != null) {

            if (!chatTest) {

                groupId = mFirebaseDatabaseReference.push().getKey();

                ChatGroup chatGroup = new ChatGroup(groupId, edMessage.getText().toString());
                mFirebaseDatabaseReference.child(CHAT_GROUP_REFERENCE).push().setValue(chatGroup);

                UserChat userChat = new UserChat(userModel.getId(), groupId, getIntent().getStringExtra("partner"), getIntent().getStringExtra("partnerId"), getIntent().getStringExtra("partnerId"));
                UserChat userChatPartner = new UserChat(getIntent().getStringExtra("partnerId"), groupId, userModel.getName(), userModel.getId(), userModel.getId());

                mFirebaseDatabaseReference.child(USER_CHAT_REFERENCE).push().setValue(userChat);
                mFirebaseDatabaseReference.child(USER_CHAT_REFERENCE).push().setValue(userChatPartner);


                Query addToCurrentUser = mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(userModel.getId());
                addToCurrentUser.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        TestModel testModel = new TestModel();
                        testModel.setPartnerId(partner);
                        testModel.setGroupId(groupId);


                        for (DataSnapshot dS: dataSnapshot.getChildren()){
                            mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).child("testModel").push().setValue(testModel);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                Query addToPartner = mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(partner);
                addToPartner.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        TestModel testModelPartner = new TestModel();
                        testModelPartner.setPartnerId(userModel.getId());
                        testModelPartner.setGroupId(groupId);


                        for (DataSnapshot dS: dataSnapshot.getChildren()){
                            mFirebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).child("testModel").push().setValue(testModelPartner);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }else{
                chatTest = true;

                updateChatGroupLastMessageFirebase("Image", groupId);
            }

            Uri photoURI = FileProvider.getUriForFile(ConversationActivity.this,
                    BuildConfig.APPLICATION_ID,
                    file);
            UploadTask uploadTask = storageReference.putFile(photoURI);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
                    @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    FileModel fileModel = new FileModel("img", downloadUrl.toString(), file.getName(), file.length() + "");
                    ChatModel chatModel = new ChatModel(userModel, "", fileModel, userModel.getId(), groupId);
                    mFirebaseDatabaseReference.child(CHAT_REFERENCE).push().setValue(chatModel);
                }
            });

        } else {
            //IS NULL
        }

        chatTest = true;
    }

    private void locationPlacesIntent() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void photoCameraIntent() {
        String nomeFoto = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();

        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory("Pictures"), nomeFoto + "camera.jpg");

        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        System.out.println(ConversationActivity.this.getFilesDir().getName());

        Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                BuildConfig.APPLICATION_ID,
                filePathImageCamera);

        it.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(it, IMAGE_CAMERA_REQUEST);
    }

    public void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(ConversationActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    ConversationActivity.this,
                    Utils.PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            // we already have permission, lets go ahead and call camera intent
            photoCameraIntent();
        }
    }

    private void photoGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);
    }

    public class MyChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvTimestamp,tvLocation;
        EmojiconTextView txtMessage;
        ImageView ivUser,ivChatPhoto;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            tvTimestamp = (TextView)itemView.findViewById(R.id.timestamp);
            txtMessage = (EmojiconTextView)itemView.findViewById(R.id.txtMessage);
            tvLocation = (TextView)itemView.findViewById(R.id.tvLocation);
            ivChatPhoto = (ImageView)itemView.findViewById(R.id.img_chat);
            ivUser = (ImageView)itemView.findViewById(R.id.ivUserChat);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            ChatModel model = mChatFirebaseAdapter.getItem(position);
            if (model.getMapModel() != null){
                mClickListenerChatFirebase.clickImageMapChat(view,position,model.getMapModel().getLatitude(),model.getMapModel().getLongitude());
            }else{
                mClickListenerChatFirebase.clickImageChat(view,position,model.getUserModel().getName(),model.getUserModel().getPhoto_profile(),model.getFile().getUrl_file());
            }
        }

        public void setTxtMessage(String message){
            if (txtMessage == null)return;
            txtMessage.setText(message);
        }

        public void setIvUser(String senderId){
            if (ivUser == null)return;


            Glide.with(ivUser.getContext())
                    .using(new FirebaseImageLoader())
                    .load(storage.getReferenceFromUrl(URL_STORAGE_REFERENCE).child(FOLDER_STORAGE_IMG).child(senderId))
                    .centerCrop()
                    .transform(new CircleImageUtil(ivUser.getContext()))
                    .override(40,40).into(ivUser);
        }

        public void setTvTimestamp(String timestamp){
            if (tvTimestamp == null)
                return;
            tvTimestamp.setText(DateUtils.getRelativeTimeSpanString(Long.parseLong(timestamp),System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        }

        public void setIvChatPhoto(String url){
            if (ivChatPhoto == null)return;

            Glide.with(ivChatPhoto.getContext())
                    .load(url)
                    .override(100, 100)
                    .fitCenter()
                    .into(ivChatPhoto);
            ivChatPhoto.setOnClickListener(this);
        }

        public void tvIsLocation(int visible){
            if (tvLocation == null)return;
            tvLocation.setVisibility(visible);
        }
    }
}