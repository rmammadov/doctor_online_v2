package com.newo.doctoronline.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.newo.doctoronline.R;
import com.newo.doctoronline.adapters.HomePagerAdapter;
import com.newo.doctoronline.adapters.HorizontalPagerAdapter;
import com.newo.doctoronline.adapters.NavigationPagerAdapter;
import com.newo.doctoronline.constants.Constants;
import com.newo.doctoronline.fragments.ConversationsFragment;
import com.newo.doctoronline.fragments.HomeCategoryFragment;
import com.newo.doctoronline.fragments.NewsFragment;
import com.newo.doctoronline.fragments.ProfileFragment;
import com.newo.doctoronline.fragments.content.DoctorCategories;
import com.newo.doctoronline.models.ConversationModel;
import com.newo.doctoronline.models.UserModel;
import com.newo.doctoronline.utils.CircleImageUtil;
import com.newo.doctoronline.utils.FirebaseUtil;
import com.newo.doctoronline.utils.NonSwipeableViewPager;
import com.newo.doctoronline.utils.SharedPreference;
import com.newo.doctoronline.utils.Utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static android.R.drawable.ic_menu_edit;
import static com.newo.doctoronline.utils.FirebaseUtil.getAssistantFirebase;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeCategoryFragment.OnListFragmentInteractionListener, ConversationsFragment.OnListFragmentInteractionListener {

    private static final String TAG = "HomeActivity";
    
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private NonSwipeableViewPager mNavigationMenuViewPager;
    private CollapsingToolbarLayout ctbLayout;
    private AppBarLayout abLayout;
    private LinearLayoutCompat llMenuHeader;
    private AppCompatTextView tvUsername;
    private ImageView ivMenuUser;
    private ImageView ivProfile;
    private AppCompatTextView tvUsernameProfile;
    public static int mfabImageResource;

    private Activity mActivity;

    private FirebaseStorage storage = FirebaseStorage.getInstance();

    private int[] tabIcons = {
            R.drawable.ic_star_24dp,
            R.drawable.ic_chats_24dp};

    private Fragment mFragment;
    private FragmentManager fragmentManager;
    private Toolbar toolbar;
    private View vProfileHeader;
    private String currentFragmentTag;
    private FirebaseUser user;
    private FloatingActionButton fabEdit;
    private String userId;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Log.w(TAG, "onCreate()");
//        Log.w(TAG, "After onCreate()" + FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl());

        //Call setup view method
        setupView();

        setRegistrationToken();
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.w(TAG, "onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.w(TAG, "onStop");

        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    /**
     * Setup View components
     */
    private void setupView() {
        //Initializations
        mAuth = FirebaseAuth.getInstance();
        SharedPreference.getInstance(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        mActivity = this;
//        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mfabImageResource = ic_menu_edit;

        fabEdit = (FloatingActionButton) findViewById(R.id.fab_home);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        ctbLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        abLayout = (AppBarLayout) findViewById(R.id.app_bar);
        navigationView = (NavigationView) findViewById(R.id.nav_view_home);
        mViewPager = (ViewPager) findViewById(R.id.viewpager_home);
        mNavigationMenuViewPager = (NonSwipeableViewPager) findViewById(R.id.viewpager_fragments_menu);
        mTabLayout = (TabLayout) findViewById(R.id.tabl_home);
        vProfileHeader = (View) findViewById(R.id.header_profile);
        View headerLayout = navigationView.getHeaderView(0);
        llMenuHeader = (LinearLayoutCompat) headerLayout.findViewById(R.id.ll_header_menu);
        tvUsername = (AppCompatTextView) headerLayout.findViewById(R.id.tv_menu_name);
        ivMenuUser = (ImageView) headerLayout.findViewById(R.id.image_rounded_menu);
        ivProfile = (ImageView) findViewById(R.id.image_rounded_profile);
        tvUsernameProfile = (AppCompatTextView) findViewById(R.id.tv_profile_name);

        //Onclick Listener method-------------------------------------------------------------------------
        setClickListeners();

        //View functionlities-----------------------------------------------------------------------
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        fragmentManager = getSupportFragmentManager();
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();
        setupNavigationViewPager(mNavigationMenuViewPager);
        mViewPager.setVisibility(View.VISIBLE);
        mNavigationMenuViewPager.setVisibility(View.GONE);


        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mfabImageResource == ic_menu_edit)
                {
                    fabEdit.setImageResource(R.drawable.ic_check);
                    mfabImageResource  = R.drawable.ic_check;
                    ((ProfileFragment)((NavigationPagerAdapter)mNavigationMenuViewPager.getAdapter()).getItem(0)).setIfEditable(true);
                }else
                {
                    ((ProfileFragment)((NavigationPagerAdapter)mNavigationMenuViewPager.getAdapter()).getItem(0)).setIfEditable(false);
                    ((ProfileFragment)((NavigationPagerAdapter)mNavigationMenuViewPager.getAdapter()).getItem(0)).updateUserData();

                    fabEdit.setImageResource(ic_menu_edit);
                    mfabImageResource  = ic_menu_edit;
                }
            }
        });

        final HorizontalInfiniteCycleViewPager horizontalInfiniteCycleViewPager =
                (HorizontalInfiniteCycleViewPager) findViewById(R.id.hicvp_home);
        horizontalInfiniteCycleViewPager.setAdapter(new HorizontalPagerAdapter(this, false));
        horizontalInfiniteCycleViewPager.startAutoScroll(true);
        horizontalInfiniteCycleViewPager.setScrollDuration(Constants.SCROLL_TIME_OUT);
    }

    private void setClickListeners() {
        // Callback registration
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    updateUserLastLogin(user.getUid());
                    userId = user.getUid();

                    SharedPreference.saveString("appUserId", user.getUid());

                    //Start Splash
                    defineState(Constants.APP_STATE_LOGGED_IN, user);
//                    startSplash();
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");

                    //Start Splash
                    defineState(Constants.APP_STATE_LOGGED_OUT, user);
                    startSplash();
                    finish();
                }
            }
        };

        mAuth.addAuthStateListener(mAuthListener);


        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openChooserWithGallery(mActivity, "Select profile image", 0);

            }
        });

        currentFragmentTag = HomeCategoryFragment.TAG;
        //Home Viewpager listener
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    currentFragmentTag = HomeCategoryFragment.TAG;
                } else {
                    currentFragmentTag = ConversationsFragment.TAG;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        llMenuHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMenuScreen(0);
            }
        });
    }

    private void updateUserLastLogin(String userId){

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        Query query = ref.child("userModel").orderByChild("userId").equalTo(userId);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot dS: dataSnapshot.getChildren()) {

                    Map<String, Object> model = new HashMap<String, Object>();

                    HashMap<String, Object> lastMsgObject = new HashMap<String, Object>();
                    lastMsgObject.put("date", ServerValue.TIMESTAMP);

                    model.put("lastLoginDate", lastMsgObject);

                    ref.child("userModel").child(dS.getRef().getKey()).updateChildren(model);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                //Handle the image
                onPhotoReturned(imageFile);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(HomeActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private void onPhotoReturned(File photoFile) {
        Log.w(TAG, "onPhotoReturned...");
        Glide.with(this)
                .load(photoFile)
                .transform(new CircleImageUtil(HomeActivity.this))
                .into(ivProfile);
    }


    /**
     * setup view pager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeCategoryFragment(), HomeCategoryFragment.TAG);
        adapter.addFrag(new ConversationsFragment(), ConversationsFragment.TAG);
        viewPager.setAdapter(adapter);
    }

    /**
     * setup navigation view pager
     *
     * @param viewPager
     */
    private void setupNavigationViewPager(ViewPager viewPager) {
        NavigationPagerAdapter adapter = new NavigationPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ProfileFragment(), ProfileFragment.TAG);
        adapter.addFrag(new NewsFragment(), NewsFragment.TAG);
        viewPager.setAdapter(adapter);
    }

    /**
     * Setup tab icons
     */
    private void setupTabIcons() {
        AppCompatTextView tabHome = (AppCompatTextView) LayoutInflater.from(this).inflate(R.layout.layout_custom_tab, null);
        tabHome.setText(getString(R.string.home_tab_title_first));
        tabHome.setCompoundDrawablesWithIntrinsicBounds(0, tabIcons[0], 0, 0);
        mTabLayout.getTabAt(0).setCustomView(tabHome);

        AppCompatTextView tabDoubts = (AppCompatTextView) LayoutInflater.from(this).inflate(R.layout.layout_custom_tab, null);
        tabDoubts.setText(getString(R.string.home_tab_title_second));
        tabDoubts.setCompoundDrawablesWithIntrinsicBounds(0, tabIcons[1], 0, 0);
        mTabLayout.getTabAt(1).setCustomView(tabDoubts);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (currentFragmentTag.equals( ProfileFragment.TAG))
            menu.findItem(R.id.action_search).setVisible(false);
        else
            menu.findItem(R.id.action_search).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                break;
            case R.id.action_notification:
                intentNotification();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            openMenuScreen(1);
        } else if (id == R.id.nav_contact) {
            openMenuScreen(2);
        } else if (id == R.id.nav_news) {
            openMenuScreen(3);
        } else if (id == R.id.nav_explore) {
            openMenuScreen(4);
        } else if (id == R.id.nav_settings) {
            openMenuScreen(5);
        } else if (id == R.id.nav_share) {
            openMenuScreen(6);
        } else if (id == R.id.nav_feedback) {
            sendMail();
        }

        return true;
    }

    private void openMenuScreen(int index) {
        if (index == 0) {
            mNavigationMenuViewPager.setCurrentItem(0);
            currentFragmentTag = ProfileFragment.TAG;
            mViewPager.setVisibility(View.GONE);
            mTabLayout.setVisibility(View.GONE);
            abLayout.setExpanded(true);
            mNavigationMenuViewPager.setVisibility(View.VISIBLE);
            vProfileHeader.setVisibility(View.VISIBLE);
            fabEdit.setVisibility(View.VISIBLE);
            this.invalidateOptionsMenu();
        } else if (index == 1) {
            currentFragmentTag = HomeCategoryFragment.TAG;
            mViewPager.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            abLayout.setExpanded(true);
            mNavigationMenuViewPager.setVisibility(View.GONE);
            vProfileHeader.setVisibility(View.GONE);
            fabEdit.setVisibility(View.GONE);
            this.invalidateOptionsMenu();
        } else if (index == 2) {
            intentAssistant();

        } else if (index == 3) {
            mNavigationMenuViewPager.setCurrentItem(1);
            currentFragmentTag = NewsFragment.TAG;
            mViewPager.setVisibility(View.GONE);
            mTabLayout.setVisibility(View.GONE);
            abLayout.setExpanded(false);
            mNavigationMenuViewPager.setVisibility(View.VISIBLE);
            vProfileHeader.setVisibility(View.GONE);
            fabEdit.setVisibility(View.GONE);
            this.invalidateOptionsMenu();
        } else if (index == 4) {
            intentExplore();
        } else if (index == 5) {
            intentSettings();
        } else if (index == 6) {

            FirebaseAuth fAuth = FirebaseAuth.getInstance();
            mAuth.signOut();

            defineState(Constants.APP_STATE_LOGGED_OUT, user);
            startSplash();
            finish();

       //     Utils.setShareIntent(this, "Test", "test");
          //  Utils.setShareIntent(this, "Test", "test");
        } else if (index == 7) {
            defineState(Constants.APP_STATE_LOGGED_OUT, user);
            startSplash();
            finish();

            Utils.setShareIntent(this, "Test", "test");
        }

        drawer.closeDrawer(GravityCompat.START);
    }

    private void intentExplore() {
        Intent intentExplore = new Intent(this, IntroActivity.class);
        startActivity(intentExplore);
    }

    private void intentAssistant(){
        UserModel assistantModel = getAssistantFirebase();
        if( null != assistantModel){
            Intent intentAssistant = new Intent(this, ConversationActivity.class);
            intentAssistant.putExtra("partnerId", assistantModel.getUserId());
            intentAssistant.putExtra("partner", assistantModel.getName());
            startActivity(intentAssistant);
        }
    }

    private void intentSettings() {
        Intent intentSettings = new Intent(this, SettingsActivity.class);
        startActivity(intentSettings);
    }

    private void intentNotification() {
        Intent intentNotification = new Intent(this, NotificationActivity.class);
        startActivity(intentNotification);
    }

    private void sendMail() {
        ShareCompat.IntentBuilder.from(this)
                .setType("message/rfc822")
                .addEmailTo("test@test.com")
                .setSubject("Feedback")
                .setText("Please write your fedback")
                //.setHtmlText(body) //If you are using HTML in your body text
                .setChooserTitle("SEND FEEDBACK")
                .startChooser();
    }

    /**
     * Start splash method
     */
    private void startSplash() {
        Intent intentSplash = new Intent(this, SplashActivity.class);
        startActivity(intentSplash);
    }

    /**
     * Define state method
     */
    private void defineState(int state, FirebaseUser user) {
        if (state == Constants.APP_STATE_LOGGED_IN) {
            Log.w(TAG, "state is: " + state);
            SharedPreference.saveInt(Constants.APP_STATE, Constants.APP_STATE_LOGGED_IN);
            setUserData(user);
        } else {
            SharedPreference.saveInt(Constants.APP_STATE, Constants.APP_STATE_LOGGED_OUT);
        }
    }

    private void setRegistrationToken(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
         //   addOrUpdateRegistrationTokenFirebase(user.getUid(), FirebaseInstanceId.getInstance().getToken());
        } else {
            // No user is signed in
        }
    }

    private void setUserData(FirebaseUser user) {

        tvUsername.setText(user.getDisplayName());
        tvUsernameProfile.setText(user.getDisplayName());

        StorageReference storageRef = storage.getReferenceFromUrl(FirebaseUtil.URL_STORAGE_REFERENCE).child(FirebaseUtil.FOLDER_STORAGE_IMG);

        storageRef.child(user.getUid()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(context)
                        .load(uri)
                        .transform(new CircleImageUtil(HomeActivity.this))
                        .into(ivMenuUser);

                Glide.with(context)
                        .load(uri)
                        .transform(new CircleImageUtil(HomeActivity.this))
                        .into(ivProfile);
            }
        });
    }

//    @Override
//    public void onListFragmentInteraction(HomeCategoryContent.DummyItem item, int position) {
//        openCategoryContent(position);
//        Log.d("OnClickCategory", "YES");
//    }


    @Override
    public void onListFragmentInteraction(DoctorCategories.Category item, int position) {

        openCategoryContent(position);
    }



    private void openCategoryContent(int position){
        Intent intentCategoryContent = new Intent(this, CategoryContentsActivity.class);
        startActivity(intentCategoryContent);
    }

    @Override
    public void onListFragmentInteraction(ConversationModel item, int position) {
        Intent intentConversation = new Intent(this, ConversationActivity.class);

        intentConversation.putExtra("partner", item.getPartnerName());
        intentConversation.putExtra("partnerId", item.getPartnerId());

        startActivity(intentConversation);
    }

    private void openConversation(int position){
        Intent intentCategoryContent = new Intent(this, ConversationActivity.class);
        intentCategoryContent.putExtra("partner","Elvin");
        intentCategoryContent.putExtra("firstTime", true);
        //intentCategoryContent.putExtra("groupId", "-KlPOdAeRyk62EdZralu");
        startActivity(intentCategoryContent);
    }
}
