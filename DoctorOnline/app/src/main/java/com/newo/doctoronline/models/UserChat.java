package com.newo.doctoronline.models;

/**
 * Created by elvinakhundzadeh on 5/25/17.
 */

public class UserChat {
    private String userId;
    private String groupId;
    private String chatPhoto;
    private String chatName;
    private String partnerId;
    private int unreadMessages;

    public UserChat(String userId, String groupId, String chatName, String chatPhoto, String partnerId) {
        this.userId = userId;
        this.groupId = groupId;
        this.chatPhoto = chatPhoto;
        this.chatName = chatName;
        this.partnerId = partnerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getChatPhoto() {
        return chatPhoto;
    }

    public void setChatPhoto(String chatPhoto) {
        this.chatPhoto = chatPhoto;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }
}
