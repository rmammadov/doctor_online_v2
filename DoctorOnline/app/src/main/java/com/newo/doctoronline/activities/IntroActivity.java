package com.newo.doctoronline.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.chyrta.onboarder.OnboarderActivity;
import com.chyrta.onboarder.OnboarderPage;
import com.newo.doctoronline.R;
import com.newo.doctoronline.constants.Constants;
import com.newo.doctoronline.utils.SharedPreference;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends OnboarderActivity {

    private static final String TAG = "IntroActivity";
    private List<OnboarderPage> onboarderPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.w(TAG, "onCreate()");

        //Setup View for the Activity
        setupView();
    }

    /**
     * Setup view method
     */
    private void setupView(){
        SharedPreference.getInstance(this);
        onboarderPages = new ArrayList<OnboarderPage>();

        // Create your first page
        OnboarderPage onboarderPage1 = new OnboarderPage(R.string.intro_title1, R.string.intro_description1, R.drawable.ic_doctor);
        OnboarderPage onboarderPage2 = new OnboarderPage(R.string.intro_title2, R.string.intro_description2, R.drawable.ic_doctor);
        OnboarderPage onboarderPage3 = new OnboarderPage(R.string.intro_title3, R.string.intro_description3, R.drawable.ic_doctor);
        OnboarderPage onboarderPage4 = new OnboarderPage(R.string.intro_title4, R.string.intro_description4, R.drawable.ic_doctor);

        // You can define title and description colors (by default white)
        onboarderPage1.setTitleColor(R.color.white);
        onboarderPage1.setDescriptionColor(R.color.white);
        onboarderPage2.setTitleColor(R.color.white);
        onboarderPage2.setDescriptionColor(R.color.white);
        onboarderPage3.setTitleColor(R.color.white);
        onboarderPage3.setDescriptionColor(R.color.white);
        onboarderPage4.setTitleColor(R.color.white);
        onboarderPage4.setDescriptionColor(R.color.white);

        // Don't forget to set background color for your page
        onboarderPage1.setBackgroundColor(R.color.colorRed);
        onboarderPage2.setBackgroundColor(R.color.colorDeepPurple);
        onboarderPage3.setBackgroundColor(R.color.colorBlue);
        onboarderPage4.setBackgroundColor(R.color.colorGreen);

        // Add your pages to the list
        onboarderPages.add(onboarderPage1);
        onboarderPages.add(onboarderPage2);
        onboarderPages.add(onboarderPage3);
        onboarderPages.add(onboarderPage4);

        // And pass your pages to 'setOnboardPagesReady' method
        setOnboardPagesReady(onboarderPages);

        //Customize tutorial screen
        setActiveIndicatorColor(android.R.color.white);
        setInactiveIndicatorColor(android.R.color.darker_gray);
        shouldDarkenButtonsLayout(true);
        setDividerColor(Color.WHITE);
        setDividerHeight(2);
        setDividerVisibility(View.VISIBLE);
        shouldUseFloatingActionButton(false);
        setSkipButtonTitle(R.string.button_skip);
        setFinishButtonTitle(R.string.button_finish);
//        setSkipButtonHidden();
//        setTitleTextSize(12);
//        setDescriptionTextSize(12);

        //Setup UI interactions
        setupUiInteractions();
    }

    /**
     * Setup Ui interactions method
     */
    private void setupUiInteractions() {
        //On click methods
    }

    @Override
    public void onSkipButtonPressed() {
        // Optional: by default it skips onboarder to the end
        super.onSkipButtonPressed();
        // Define your actions when the user press 'Skip' button
    }

    @Override
    public void onFinishButtonPressed() {
        Log.w(TAG, "onFinishButtonPressed()");
        if(SharedPreference.getInt(Constants.APP_STATE) == 0) {
            Intent intentHome = new Intent(this, LoginOptionsActivity.class);
            startActivity(intentHome);
            finish();
        }else{
            finish();
        }
    }
}
