package com.newo.doctoronline.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elvinakhundzadeh on 6/18/17.
 */

public class UserData {

    private String intro;
    private String contact;
    private String userId;
    private Map<String, String> specialties;
    private LocationModel location;

    public UserData(String intro, String contact, HashMap<String, String> specialties, LocationModel location) {
        this.location = new LocationModel(40.4093, 49.8671);
        this.intro = intro;
        this.contact = contact;
        this.specialties = new HashMap<>();
        this.location = location;
    }

    public UserData(){

    }

    public UserData(String userId) {
        this.userId = userId;
        this.location = new LocationModel(40.4093, 49.8671);
        this.intro = "";
        this.contact = "";
        this.specialties = new HashMap<>();
      //  this.specialties.put("Test","Test");
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocationModel getLocation() {
        return location;
    }

    public void setLocation(LocationModel location) {
        this.location = location;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Map<String, String> getSpecialties() {
        return specialties;
    }

    public void setSpecialties(Map<String, String> specialties) {
        this.specialties = specialties;
    }
}
