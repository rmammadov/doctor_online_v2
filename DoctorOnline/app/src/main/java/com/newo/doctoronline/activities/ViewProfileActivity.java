package com.newo.doctoronline.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.newo.doctoronline.R;
import com.newo.doctoronline.adapters.ViewProfilePagerAdapter;
import com.newo.doctoronline.fragments.ViewProfileFragment;

public class ViewProfileActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        //Setup Views and components method
        setupView();
    }


    /**
     * Setup View components method
     */
    private void setupView() {
        //Setup Components
        toolbar = (Toolbar) findViewById(R.id.toolbar_view_profile);
        mViewPager = (ViewPager) findViewById(R.id.viewpager_view_profile);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Setup Pager
        setupViewPager(mViewPager);
    }

    private void setClickListeners() {

    }

    /**
     * setup view pager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewProfilePagerAdapter adapter = new ViewProfilePagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ViewProfileFragment(), ViewProfileFragment.TAG);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
