package com.newo.doctoronline.models;

import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * Created by elvinakhundzadeh on 5/25/17.
 */

public class ChatGroup {
    private String id;
    private String lastMessage;
    private String lastMessageDate;
    private HashMap<String, Object> lastMsgDate;

    public ChatGroup(String id, String lastMessage) {
        this.id = id;
        this.lastMessage = lastMessage;

        HashMap<String, Object> lastMsgObject = new HashMap<String, Object>();
        lastMsgObject.put("date", ServerValue.TIMESTAMP);

        this.lastMsgDate = lastMsgObject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(String lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public HashMap<String, Object> getLastMsgDate() {
        return lastMsgDate;
    }

    public void setLastMsgDate(HashMap<String, Object> lastMsgDate) {
        this.lastMsgDate = lastMsgDate;
    }
}
