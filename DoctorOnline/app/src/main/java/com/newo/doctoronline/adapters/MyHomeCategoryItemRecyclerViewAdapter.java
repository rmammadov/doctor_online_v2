package com.newo.doctoronline.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.newo.doctoronline.R;
import com.newo.doctoronline.fragments.HomeCategoryFragment.OnListFragmentInteractionListener;
import com.newo.doctoronline.fragments.content.DoctorCategories;
import com.newo.doctoronline.fragments.dummy.HomeCategoryContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyHomeCategoryItemRecyclerViewAdapter extends RecyclerView.Adapter<MyHomeCategoryItemRecyclerViewAdapter.ViewHolder> {

    private final List<DoctorCategories.Category> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context context;
    private RelativeLayout rlHomeCategoryListItem;

    public MyHomeCategoryItemRecyclerViewAdapter(List<DoctorCategories.Category> items, OnListFragmentInteractionListener listener, Context mContext) {
        mValues = items;
        mListener = listener;
        context = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_home_category_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);

        holder.mRlHomeCategoryListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem, position);




                    Toast.makeText(context, "Heyoo", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final RelativeLayout mRlHomeCategoryListItem;
        public DoctorCategories.Category mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.tv_conversation_list_item_name);
            mContentView = (TextView) view.findViewById(R.id.tv_conversation_list_item_description);
            mRlHomeCategoryListItem = (RelativeLayout) view.findViewById(R.id.rl_home_category_list_item);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
