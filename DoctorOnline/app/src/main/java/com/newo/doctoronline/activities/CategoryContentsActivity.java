package com.newo.doctoronline.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.newo.doctoronline.R;
import com.newo.doctoronline.adapters.CategoryContentsAdapter;
import com.newo.doctoronline.fragments.CategoryContentsFragment;
import com.newo.doctoronline.models.UserModel;
import com.newo.doctoronline.utils.NonSwipeableViewPager;



public class CategoryContentsActivity extends AppCompatActivity implements CategoryContentsFragment.OnListFragmentInteractionListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;

    public final static String TAG = "CategoryContentsActivity";
    //Components
    private NonSwipeableViewPager mViewPager;
    private Toolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_contents);

        setupView();
    }

    /**
     * Setup View components method
     */
    private void setupView() {
        //Setup Components
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_category_contents);
        setSupportActionBar(myToolbar);
        mViewPager = (NonSwipeableViewPager) findViewById(R.id.viewpager_category_contents);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        viewPagerSetup(mViewPager);

        mViewPager.setCurrentItem(0);
    }

    /**
     * setup view pager
     *
     * @param viewPager
     */

    private void viewPagerSetup(ViewPager viewPager){
        CategoryContentsAdapter adapter = new CategoryContentsAdapter(getSupportFragmentManager());
        adapter.addFrag(new CategoryContentsFragment(), CategoryContentsFragment.TAG);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onListFragmentInteraction(UserModel item, int postion) {
        openViewProfileActivity(item);
    }

    private void openViewProfileActivity(UserModel item){

        //TODO GET IT BACK
       // Intent intentViewProfile = new Intent(this, ViewProfileActivity.class);
       // startActivity(intentViewProfile);


        Intent intentCategoryContent = new Intent(this, ConversationActivity.class);

        intentCategoryContent.putExtra("partner", item.getName());
        intentCategoryContent.putExtra("partnerId", item.getUserId());


        //intentCategoryContent.putExtra("groupId", "-KlPOdAeRyk62EdZralu");
        startActivity(intentCategoryContent);
    }
}
