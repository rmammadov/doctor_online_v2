package com.newo.doctoronline.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.DrawableCrossFadeFactory;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.bumptech.glide.request.target.Target;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by rmammadov on 9/6/16.
 */
public class Utils {




    public static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    private final static View.OnClickListener mOnImageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
//            IntentHelper.openUrlInBrowser(view.getContext(), (String) view.getTag(view.getId()));
        }
    };

    public static void setupImage(final Context context, final ImageView imageView, final LibraryObject libraryObject) {
        setupImage(context, imageView, libraryObject, -1);
    }

    public static void setupImage(final Context context, final ImageView imageView, final LibraryObject libraryObject, final int position) {

        imageView.setTag(imageView.getId(), libraryObject.getUrl());
        imageView.setOnClickListener(mOnImageClickListener);

        Glide.with(context)
                .load(position == -1 ? libraryObject.getImage() : libraryObject.getImages()[position])
                .asBitmap()
                .centerCrop()
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(final Exception e, final String model, final Target<Bitmap> target, final boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(final Bitmap resource, final String model, final Target<Bitmap> target, final boolean isFromMemoryCache, final boolean isFirstResource) {
                        final ImageViewTarget imageViewTarget = (ImageViewTarget) target;
                        return new DrawableCrossFadeFactory<>()
                                .build(isFromMemoryCache, isFirstResource)
                                .animate(
                                        new BitmapDrawable(
                                                imageViewTarget.getView().getResources(),
                                                resource
                                        ),
                                        imageViewTarget
                                );
                    }
                })
                .into(imageView);
    }

    /**
     * Share intent
     *
     * @param activty
     * @param title
     * @param body
     */
    public static void setShareIntent(AppCompatActivity activty, String title, String body) {
        String shareBody = body;
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        activty.startActivity(sharingIntent);
    }

    public static void intentRate(AppCompatActivity activity, String title, String body) {

    }

    public static boolean checkInternetConnection(Context context) {
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        conectado = conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected();
        return conectado;
    }

    public static void initToast(Context c, String message){
        Toast.makeText(c,message,Toast.LENGTH_SHORT).show();
    }

    public static String local(String latitudeFinal,String longitudeFinal){
        return "https://maps.googleapis.com/maps/api/staticmap?center="+latitudeFinal+","+longitudeFinal+"&zoom=18&size=280x280&markers=color:red|"+latitudeFinal+","+longitudeFinal;
    }

    public static byte[] readFully(InputStream input) throws IOException
    {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
        return output.toByteArray();
    }
}
