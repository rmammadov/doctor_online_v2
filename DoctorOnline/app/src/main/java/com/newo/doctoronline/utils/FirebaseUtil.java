package com.newo.doctoronline.utils;

import android.net.Uri;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.newo.doctoronline.models.ChatGroup;
import com.newo.doctoronline.models.ChatModel;
import com.newo.doctoronline.models.RegistrationTokenModel;
import com.newo.doctoronline.models.UserChat;
import com.newo.doctoronline.models.UserData;
import com.newo.doctoronline.models.UserModel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elvinakhundzadeh on 5/18/17.
 */

public class FirebaseUtil {

    public static final String URL_STORAGE_REFERENCE = "gs://doctor-online-15294.appspot.com/";
    public static final String FOLDER_STORAGE_IMG = "images";
    public static final String CHAT_REFERENCE = "chatmodel";
    public static final String CHAT_GROUP_REFERENCE = "chatGroup";
    public static final String USER_CHAT_REFERENCE = "userChat";
    public static final String USER_MODEL_REFERENCE = "userModel";
    public static final String USER_DATA_REFERENCE = "userData";
    public static final String REGISTRATION_TOKEN_REFERENCE = "regToken";
    public static final FirebaseStorage storage = FirebaseStorage.getInstance();

    private static int userAddResult = 0;

    private static Uri photoUri;

    private static UserModel assistantModel;

    public static final int USER_PHONE_AUTH_EXISTS = 1;
    public static final int USER_PHONE_AUTH_OK = 0;

    public static final DatabaseReference firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();;

    public static void addMessageFirebase(String groupId, String message, UserModel userModel, String receiverId){

        String chatId = firebaseDatabaseReference.push().getKey();

        ChatModel chatModel = new ChatModel();

        chatModel.setId(chatId);
        chatModel.setGroupId(groupId);
        chatModel.setMessage(message);
        chatModel.setSenderId(userModel.getId());
        chatModel.setUserModel(userModel);
        chatModel.setReceiverId(receiverId);

        firebaseDatabaseReference.child(FirebaseUtil.CHAT_REFERENCE).push().setValue(chatModel);
    }

    public static void updateChatGroupLastMessageFirebase(final String lastMessage, final String groupId){

        Query query = firebaseDatabaseReference.child(CHAT_GROUP_REFERENCE).orderByChild("id").equalTo(groupId);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot dS: dataSnapshot.getChildren()) {

                    Map<String, Object> model = new HashMap<String, Object>();
                    model.put("lastMessage",  lastMessage);
                    model.put("id", groupId);

                    HashMap<String, Object> lastMsgObject = new HashMap<String, Object>();
                    lastMsgObject.put("date", ServerValue.TIMESTAMP);

                    model.put("lastMessageDate", lastMsgObject);

                    firebaseDatabaseReference.child("chatGroup").child(dS.getRef().getKey()).updateChildren(model);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void addChatModelFirebase(ChatModel chatModel){
        firebaseDatabaseReference.child(CHAT_REFERENCE).push().setValue(chatModel);
    }

    public static void addChatGroupFirebase(ChatGroup chatGroup){
        firebaseDatabaseReference.child(CHAT_GROUP_REFERENCE).push().setValue(chatGroup);
    }

    public static void addUserChatFirebase(UserChat userChat){
        firebaseDatabaseReference.child(USER_CHAT_REFERENCE).push().setValue(userChat);
    }

    public static void addUserDataFirebase(UserData userData){
        firebaseDatabaseReference.child(USER_DATA_REFERENCE).push().setValue(userData);
    }

    public static int addPhoneAuthUserFirebase(final UserModel userModel){

        /*Query query = firebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("phone").equalTo(userModel.getPhone());

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    userAddResult = USER_PHONE_AUTH_EXISTS;
                }else{
                    userAddResult = USER_PHONE_AUTH_OK;
                    addUserFirebase(userModel);
                    addUserDataFirebase(new UserData(userModel.getUserId()));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/

        addUserFirebase(userModel);
        addUserDataFirebase(new UserData(userModel.getUserId()));

        return userAddResult;
    }

    public static void addUserFirebase(UserModel userModel){
        firebaseDatabaseReference.child(USER_MODEL_REFERENCE).push().setValue(userModel);
    }

    public static int checkPhoneFirebase(String phoneNumber){
        Query query = firebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("phone").equalTo(phoneNumber);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    userAddResult = USER_PHONE_AUTH_EXISTS;
                    System.out.println("Helvaci" + userAddResult);
                }else{
                    userAddResult = USER_PHONE_AUTH_OK;
                    System.out.println("Helvaci_2" + userAddResult);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return userAddResult;
    }

    public static void updateUserDataFirebase(final String userId, final UserData userData){

        Query query = firebaseDatabaseReference.child(USER_DATA_REFERENCE).orderByChild("userId").equalTo(userId);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dS: dataSnapshot.getChildren()) {

                    Map<String, Object> model = new HashMap<String, Object>();
                    model.put("contact",  userData.getContact());
                    model.put("intro", userData.getIntro());
                    model.put("location", userData.getLocation());  //TODO
                    model.put("specialties", new HashMap<String, String>());  //TODO

                    firebaseDatabaseReference.child(USER_DATA_REFERENCE).child(dS.getRef().getKey()).updateChildren(model);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static UserModel getAssistantFirebase() {
        Query query = firebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userType").equalTo("99");

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String uniqueKey = "";

                    for (DataSnapshot dS : dataSnapshot.getChildren()) {

                        uniqueKey = firebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).getKey();
                        break;
                    }

                    assistantModel = dataSnapshot.child(uniqueKey).getValue(UserModel.class);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return assistantModel;

    }

    public static void addOrUpdateRegistrationTokenFirebase(final String userId, final String  token){

        Query query = firebaseDatabaseReference.child(REGISTRATION_TOKEN_REFERENCE).orderByChild("userId").equalTo(userId);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    for(DataSnapshot dS: dataSnapshot.getChildren()){
                        Map<String, Object> model = new HashMap<>();
                        model.put("token", token);
                        firebaseDatabaseReference.child(REGISTRATION_TOKEN_REFERENCE).child(dS.getRef().getKey()).updateChildren(model);
                    }
                }
                else{

                    RegistrationTokenModel registrationTokenModel = new RegistrationTokenModel(userId, token);

                    firebaseDatabaseReference.child(REGISTRATION_TOKEN_REFERENCE).push().setValue(registrationTokenModel);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public static Uri getProfilePhoto(String userId){

        System.out.println("getProfilePhoto userId: " + userId);

        StorageReference storageReference = storage.getReferenceFromUrl(URL_STORAGE_REFERENCE).child(FOLDER_STORAGE_IMG).child(userId);

        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                System.out.println("getProfilePhoto photoUri: " + photoUri);
                photoUri = uri;
            }
        });
        return photoUri;
    }

    public static void updateUserModelFirebase(String userId, final UserModel userModel){

        Query query = firebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo(userId);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot dS: dataSnapshot.getChildren()){
                    Map<String, Object> model = new HashMap<String, Object>();

                    model.put("name", userModel.getName());
                    model.put("password", userModel.getPassword());

                    firebaseDatabaseReference.child(USER_MODEL_REFERENCE).child(dS.getRef().getKey()).updateChildren(model);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private static void emptyFirebase(){

        firebaseDatabaseReference.child(USER_CHAT_REFERENCE).removeValue();
        firebaseDatabaseReference.child(CHAT_REFERENCE).removeValue();
        firebaseDatabaseReference.child(CHAT_GROUP_REFERENCE).removeValue();

        firebaseDatabaseReference.setValue(null);
    }
}
