package com.newo.doctoronline.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.newo.doctoronline.constants.Constants;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by rmammadov on 10/22/16.
 */

public class SharedPreference {

    private static SharedPreferences.Editor editor;
    private static SharedPreferences prefs;

    public static void getInstance(Context context){
        prefs = context.getSharedPreferences(Constants.PREFERENCE_NAME_MAIN, MODE_PRIVATE);
        editor = prefs.edit();
    }

    public static void saveInt(String key, int value){
        editor.putInt(key, value);
        editor.apply();
    }

    public static void saveBoolean(String key, boolean value){
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void saveString(String key, String value){
        editor.putString(key, value);
        editor.apply();
    }

    public static int getInt(String key){
        return prefs.getInt(key, 0);
    }

    public static boolean getBoolean(String key){
        return prefs.getBoolean(key, false);
    }

    public static String getString(String key){
        return prefs.getString(key, "no_data");
    }

    public static void clearSharedPreference(){
        editor.clear();
        editor.apply();
    }
}
