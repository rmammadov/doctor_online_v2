package com.newo.doctoronline.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anton46.collectionitempicker.CollectionPicker;
import com.anton46.collectionitempicker.Item;
import com.anton46.collectionitempicker.OnItemClickListener;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.newo.doctoronline.R;
import com.newo.doctoronline.models.LocationModel;
import com.newo.doctoronline.models.UserData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.newo.doctoronline.utils.FirebaseUtil.USER_DATA_REFERENCE;
import static com.newo.doctoronline.utils.FirebaseUtil.updateUserDataFirebase;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    public final static String TAG = "ProfileFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private MapView mMapView;
    private GoogleMap googleMap;
    private AppCompatTextView tvAddress;
    private AppCompatEditText edPhone;
    private AppCompatEditText etIntro;
    private CollectionPicker picker;
    private static int PLACE_PICKER_REQUEST = 1;

    private LinearLayoutCompat pickerHolder;
    private Map<String, String> specialties;

    //BAKU for exceptional case only...
    private double userLocationLat = 40.4093;
    private double userLocationLong  = 49.8671;

    List<Item> items = new ArrayList<>();;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        setupView(view, savedInstanceState);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void setupView(View view, Bundle savedInstanceState) {
        mMapView = (MapView) view.findViewById(R.id.map_profile);
        tvAddress = (AppCompatTextView) view.findViewById(R.id.tv_address_profile);
        edPhone = (AppCompatEditText) view.findViewById(R.id.tv_contact_profile);
        etIntro = (AppCompatEditText) view.findViewById(R.id.et_intro_profile);
        mMapView.onCreate(savedInstanceState);
        picker = (CollectionPicker) view.findViewById(R.id.collection_item_picker);

        pickerHolder = (LinearLayoutCompat) view.findViewById(R.id.color_picker_holder);


        List<Item> items = new ArrayList<>();
        items.add(new Item("item 1", "Item 1"));
        items.add(new Item("item 2", "Item 2"));
        items.add(new Item("item 3", "Item 3"));

        picker.setItems(items);
        picker.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onClick(Item item, int position) {

            }
        });

        mMapView.onResume(); // needed to get the map to display immediately

        tvAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        setIfEditable(false);
        getMapReadyCallback();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();


        ref.child(USER_DATA_REFERENCE).orderByChild("userId").equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String uniqueKey = "";
                for(DataSnapshot dS: dataSnapshot.getChildren()){
                    uniqueKey = ref.child(USER_DATA_REFERENCE).child(dS.getRef().getKey()).getKey();
                    break;

                }
                if(dataSnapshot.exists()){
                    UserData userData = dataSnapshot.child(uniqueKey).getValue(UserData.class);

                    LocationModel locationModel = userData.getLocation();

                    userLocationLat = locationModel.getLat();
                    userLocationLong = locationModel.getLng();

                    getMapReadyCallback();

                    edPhone.setText(userData.getContact());
                    etIntro.setText(userData.getIntro());

                    return;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PLACE_PICKER_REQUEST){
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(), data);
                if (place != null) {

                    LatLng latLng = place.getLatLng();

                    userLocationLong = latLng.longitude;
                    userLocationLat = latLng.latitude;

                    getMapReadyCallback();
                } else {

                }
            }
        }
    }

    private void getMapReadyCallback(){
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

                    return;
                }

                // For dropping a marker at a point on the Map
              //  LatLng sydney = new LatLng(40.4093, 49.8671);
                LatLng sydney = new LatLng(userLocationLat, userLocationLong);
                googleMap.addMarker(new MarkerOptions().position(sydney));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                getAddressName(sydney);
            }
        });
    }

    private void getAddressName(LatLng location){
        try {
            Geocoder geo = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(location.latitude, location.longitude, 1);
            if (addresses.isEmpty()) {
                tvAddress.setText("Waiting for location...");
            }
            else {
                if (addresses.size() > 0) {
                    tvAddress.setText(addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() +", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName());
                    //Toast.makeText(getApplicationContext(), "Address:- " + addresses.get(0).getFeatureName() + addresses.get(0).getAdminArea() + addresses.get(0).getLocality(), Toast.LENGTH_LONG).show();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * Ask for permission if required method
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Get map ready callback method
                    getMapReadyCallback();

                } else {

                }
                return;
            }
        }
    }

    public void setIfEditable(boolean visible){
        etIntro.setFocusable(visible);
        etIntro.setFocusableInTouchMode(visible);
        etIntro.setClickable(visible);

        edPhone.setFocusable(visible);
        edPhone.setFocusableInTouchMode(visible);
        edPhone.setClickable(visible);

        tvAddress.setClickable(visible);

        pickerHolder.setClickable(visible);
        pickerHolder.setFocusable(visible);
        pickerHolder.setFocusableInTouchMode(visible);

    }

    public void updateUserData(){
        LocationModel locationModel = new LocationModel(userLocationLat, userLocationLong);
        UserData userData = new UserData(etIntro.getText().toString().trim(), edPhone.getText().toString().trim(), null, locationModel);
        updateUserDataFirebase(FirebaseAuth.getInstance().getCurrentUser().getUid(), userData);
    }
}
