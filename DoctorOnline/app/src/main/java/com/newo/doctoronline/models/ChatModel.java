package com.newo.doctoronline.models;

import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * Created by elvinakhundzadeh on 5/23/17.
 */

public class ChatModel {
    private String id;
    private UserModel userModel;
    private String message;
    private FileModel file;
    private MapModel mapModel;
    private String groupId;
    private String senderId;
    private String receiverId;
    private HashMap<String, Object> timeStamp;


    public ChatModel() {

        HashMap<String, Object> lastMsgObject = new HashMap<String, Object>();
        lastMsgObject.put("date", ServerValue.TIMESTAMP);

        this.timeStamp = lastMsgObject;
    }

    public ChatModel(UserModel userModel, String message, FileModel file, String senderId, String groupId) {
        this.userModel = userModel;
        this.message = message;
        this.file = file;
        this.senderId = senderId;
        this.groupId = groupId;

        HashMap<String, Object> lastMsgObject = new HashMap<String, Object>();
        lastMsgObject.put("date", ServerValue.TIMESTAMP);

        this.timeStamp = lastMsgObject;
    }

    public ChatModel(UserModel userModel, MapModel mapModel, String senderId, String groupId) {
        this.userModel = userModel;
        this.mapModel = mapModel;
        this.senderId = senderId;
        this.groupId = groupId;

        HashMap<String, Object> lastMsgObject = new HashMap<String, Object>();
        lastMsgObject.put("date", ServerValue.TIMESTAMP);

        this.timeStamp = lastMsgObject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FileModel getFile() {
        return file;
    }

    public void setFile(FileModel file) {
        this.file = file;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public HashMap<String, Object> getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(HashMap<String, Object> timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "ChatModel{" +
                "mapModel=" + mapModel +
                ", file=" + file +
                ", timeStamp='" + timeStamp + '\'' +
                ", message='" + message + '\'' +
                ", userModel=" + userModel +
                '}';
    }
}
