package com.newo.doctoronline.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.newo.doctoronline.R;
import com.newo.doctoronline.fragments.ConversationsFragment.OnListFragmentInteractionListener;
import com.newo.doctoronline.fragments.dummy.ConversationsContent.DummyItem;
import com.newo.doctoronline.models.ConversationModel;
import com.newo.doctoronline.utils.CircleImageUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import static com.newo.doctoronline.utils.FirebaseUtil.FOLDER_STORAGE_IMG;
import static com.newo.doctoronline.utils.FirebaseUtil.URL_STORAGE_REFERENCE;
import static com.newo.doctoronline.utils.FirebaseUtil.storage;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyConversationsItemRecyclerViewAdapter extends RecyclerView.Adapter<MyConversationsItemRecyclerViewAdapter.ViewHolder> {

    private final List<ConversationModel> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context mContext;

    public MyConversationsItemRecyclerViewAdapter(List<ConversationModel> items, OnListFragmentInteractionListener listener, Context Context) {
        mValues = items;
        mListener = listener;
        mContext = Context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_conversations_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getPartnerName());
        holder.mContentView.setText(mValues.get(position).getLastMessage());
      //  holder.mTimestamp.setText(mValues.get(position).getLastMessageDate().get("date").toString());

        try{
            DateFormat sfd = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

            holder.mTimestamp.setText((DateUtils.getRelativeTimeSpanString(Long.parseLong(mValues.get(position).getLastMessageDate().get("date").toString()), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS)));
        }catch (Exception e){
            e.printStackTrace();
        }
     //   holder.mTimestamp.setText(new SimpleDateFormat("yyyy-MM-dd").format(Integer.parseInt(mValues.get(position).getLastMessageDate().get("date").toString())));
       if(!mValues.get(position).getUnreadMessagesCount().equals("0")) {
           holder.mUnreadMessages.setText(mValues.get(position).getUnreadMessagesCount());
           holder.mUnreadMessages.setBackgroundResource(R.drawable.shape_counter_indicator_circile_primary);
       }

        Glide.with(holder.mChatPhoto.getContext())
                .using(new FirebaseImageLoader())
                .load(storage.getReferenceFromUrl(URL_STORAGE_REFERENCE).child(FOLDER_STORAGE_IMG).child(mValues.get(position).getPhoto()))
                .centerCrop()
                .transform(new CircleImageUtil(holder.mChatPhoto.getContext()))
                .override(40,40)
                .into(holder.mChatPhoto);

        holder.mRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem, position);

                    Toast.makeText(mContext, "Chat Clicked " + holder.mItem.getPartnerId(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mTimestamp;
        public final TextView mUnreadMessages;
        public final ImageView mChatPhoto;
        public final RelativeLayout mRelativeLayout;
        public ConversationModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.tv_conversation_list_item_name);
            mContentView = (TextView) view.findViewById(R.id.tv_conversation_list_item_description);
            mTimestamp = (TextView) view.findViewById(R.id.tv_conversations_list_item_time);
            mUnreadMessages = (TextView) view.findViewById(R.id.tv_conversations_list_item_seen);
            mChatPhoto = (ImageView) view.findViewById(R.id.image_rounded_list_item_conversation);
            mRelativeLayout = (RelativeLayout) view.findViewById(R.id.rl_layout_conversations_item);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
