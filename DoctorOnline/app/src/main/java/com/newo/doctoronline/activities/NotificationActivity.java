package com.newo.doctoronline.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.newo.doctoronline.R;
import com.newo.doctoronline.adapters.NotificationAdapter;
import com.newo.doctoronline.models.NotificationModel;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    private List<NotificationModel> listNotification = new ArrayList<>();
    private RecyclerView recyclerViewNotification;
    private NotificationAdapter mAdapterNotification;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        //Call method to setup veiw and components after creating activity window
        setupView();
    }

    /**
     * Setup View components
     */
    private void setupView() {
        //Initalize Components
        toolbar = (Toolbar) findViewById(R.id.toolbar_notifications);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerViewNotification = (RecyclerView) findViewById(R.id.rv_notifications);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewNotification.setLayoutManager(mLayoutManager);
        recyclerViewNotification.setItemAnimator(new DefaultItemAnimator());
        mAdapterNotification = new NotificationAdapter(listNotification);
        recyclerViewNotification.setAdapter(mAdapterNotification);

        //Set on clickllisteners of the components
        setOnClickListeners();

        //Call Leaderboard recyclerView method
        prepareNotificationList();
    }

    /**
     * Set on click listeners of the components
     */
    private void setOnClickListeners(){
        //Back press of the toolbar and finish notification acitivty to back pervious acitivty
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
    }
    /**
     * Set notification list to recyclerView
     */
    private void prepareNotificationList() {
        NotificationModel notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", true);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", true);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", true);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", false);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", false);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", false);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", false);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", false);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", false);
        listNotification.add(notificationModel);

        notificationModel = new NotificationModel("Practice 2: Rotational Motion","Yesterday", false);
        listNotification.add(notificationModel);

        mAdapterNotification.notifyDataSetChanged();
    }
}
