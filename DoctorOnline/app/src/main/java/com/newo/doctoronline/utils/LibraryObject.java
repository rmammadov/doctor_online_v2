package com.newo.doctoronline.utils;

/**
 * Created by rmammadov on 9/6/16.
 */
public class LibraryObject {
    private String mUrl;
    private String mImage;
    private String[] mImages = null;

    public LibraryObject(final String url, final String image) {
        mUrl = url;
        mImage = image;
    }

    public LibraryObject(final String url, final String[] images) {
        mUrl = url;
        mImages = images;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getImage() {
        return mImage;
    }

    public String[] getImages() {
        return mImages;
    }
}

