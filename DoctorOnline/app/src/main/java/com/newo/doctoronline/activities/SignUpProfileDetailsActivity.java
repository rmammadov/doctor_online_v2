package com.newo.doctoronline.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.newo.doctoronline.R;
import com.newo.doctoronline.models.UserData;
import com.newo.doctoronline.models.UserModel;
import com.newo.doctoronline.utils.CircleImageUtil;
import com.newo.doctoronline.utils.FirebaseUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.newo.doctoronline.utils.FirebaseUtil.addUserDataFirebase;
import static com.newo.doctoronline.utils.FirebaseUtil.storage;
import static com.newo.doctoronline.utils.FirebaseUtil.updateUserModelFirebase;

public class SignUpProfileDetailsActivity extends AppCompatActivity {

    private ImageView imageCircle;
    private AppCompatButton btnSignUp;
    private AppCompatEditText etName;
    private AppCompatEditText etSurname;
    private AppCompatEditText etPassword;
    private AppCompatEditText etPasswordRepeat;
    private String mPhoneNumber;
    private String appUserId;
    private String userId;
    private ProgressDialog mProgressDialog;
    private Activity mActivity;

    private Uri mProfilePhotoUri;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private UserModel userModel;

    private Context mContext;

    private static final String TAG = "ProfileDetailsActivity";
    private DatabaseReference mFirebaseDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_profile_details);

        setupView();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mAuthListener != null) {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void setupView(){

        mProgressDialog = new ProgressDialog(this);
        etName = (AppCompatEditText) findViewById(R.id.etxt_name);
        etSurname = (AppCompatEditText) findViewById(R.id.etxt_surname);
        etPassword = (AppCompatEditText) findViewById(R.id.etxt_password);
        etPasswordRepeat = (AppCompatEditText) findViewById(R.id.etxt_password_repeat);

        mPhoneNumber = getIntent().getStringExtra("phoneNumber");
        appUserId = getIntent().getStringExtra("appUserId");

        mContext = SignUpProfileDetailsActivity.this;
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        imageCircle = (ImageView) findViewById(R.id.image_rounded_signup);
        btnSignUp = (AppCompatButton) findViewById(R.id.btn_signup);

        mActivity = this;

        imageCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openChooserWithGallery(mActivity, "Select profile image", 0);
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userId = mFirebaseDatabaseReference.push().getKey();

                if(!etPassword.getText().toString().trim().equals("") && etPassword.getText().toString().trim().equals(etPasswordRepeat.getText().toString().trim())){

                    System.out.println("Condition ok.");
                    userModel = new UserModel();
                    userModel.setName(etName.getText().toString()  + " " + etSurname.getText().toString());
                    userModel.setPassword(etPassword.getText().toString().trim());

                    updateUserModelFirebase(appUserId, userModel);
                    addUserDataFirebase(new UserData(appUserId));

                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(userModel.getName())
                            .build();

                    FirebaseAuth.getInstance().getCurrentUser().updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                addPhotoStorage(appUserId);
                              //  openHomePage();
                            }
                            else {
                                throw new Error(task.getException().getMessage(),task.getException().getCause());
                            }
                        }
                    });
                }else{
                    Toast.makeText(mContext, "Passwords are incorrect", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    private void openHomePage() {
        Intent intentHome = new Intent(this, HomeActivity.class);
        startActivity(intentHome);
        finish();
    }

    private void addPhotoStorage(String userId){

        StorageReference storageRef = storage.getReferenceFromUrl(FirebaseUtil.URL_STORAGE_REFERENCE).child(FirebaseUtil.FOLDER_STORAGE_IMG);

        StorageReference storageReference = storageRef.child(userId);

        imageCircle.setDrawingCacheEnabled(true);
        imageCircle.buildDrawingCache();

        try {
            byte[] photoByteArray = profilePhoto();

            System.out.println("Before uploading image: " + userId);

            UploadTask uploadTask = storageReference.putBytes(photoByteArray);
            System.out.println("After uploading image: " + userId);


            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG , e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "sendFileFirebase");
                    mProfilePhotoUri = taskSnapshot.getDownloadUrl();

                   // Uri downloadUrl = taskSnapshot.getDownloadUrl();
                }
            });

            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setPhotoUri(mProfilePhotoUri)
                    .build();

            FirebaseAuth.getInstance().getCurrentUser().updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    openHomePage();
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private byte[] profilePhoto(){
        Bitmap bitmap = imageCircle.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageInByte = baos.toByteArray();

        return imageInByte;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                //Handle the image
                onPhotoReturned(imageFile);
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(SignUpProfileDetailsActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    private void onPhotoReturned(File photoFile) {
        Glide.with(this)
                .load(photoFile)
                .transform(new CircleImageUtil(SignUpProfileDetailsActivity.this))
                .into(imageCircle);
    }
}
