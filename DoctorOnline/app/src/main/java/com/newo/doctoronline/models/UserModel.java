package com.newo.doctoronline.models;

import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * Created by elvinakhundzadeh on 5/23/17.
 */

public class UserModel {

    private String id;
    private String name;
    private String photo_profile;
    private String facebookToken;
    private String city;
    private String userType;
    private String password;
 //   private String lastLoginDate;
    private String loginType;
    private String email;
    private String userId;
    private String phone;
    private TestModel testModel;
    private HashMap<String, Object> lastLoginDate;

    public UserModel() {

        HashMap<String, Object> lastMsgObject = new HashMap<String, Object>();
        lastMsgObject.put("date", ServerValue.TIMESTAMP);

        this.lastLoginDate = lastMsgObject;
    }


    public TestModel getTestModel() {
        return testModel;
    }

    public void setTestModel(TestModel testModel) {
        testModel = testModel;
    }

    public UserModel(String name, String id) {
        this.name = name;
        this.photo_profile = id;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto_profile() {
        return photo_profile;
    }

    public void setPhoto_profile(String photo_profile) {
        this.photo_profile = photo_profile;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

   /* public String getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }*/

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}