package com.newo.doctoronline.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.newo.doctoronline.R;
import com.newo.doctoronline.adapters.MyConversationsItemRecyclerViewAdapter;
import com.newo.doctoronline.models.ConversationModel;
import com.newo.doctoronline.utils.FirebaseUtil;
import com.newo.doctoronline.utils.SharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ConversationsFragment extends Fragment {

    public final static String TAG = "ConversationsFragment";
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private MyConversationsItemRecyclerViewAdapter adapter;
    private View view;

    private MyConversationsItemRecyclerViewAdapter mAdapter;

    private List<ConversationModel> mConversationModelList = new ArrayList<>();
    private String appUserId;
    private ConversationModel conversationModel;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ConversationsFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ConversationsFragment newInstance(int columnCount) {
        ConversationsFragment fragment = new ConversationsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            mConversationModelList.clear();
            new FirebaseTask().execute();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_conversations, container, false);

        System.out.println("Current user's ID: " + SharedPreference.getString("appUserId"));

        setupView();

        new FirebaseTask().execute();

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyConversationsItemRecyclerViewAdapter(mConversationModelList, mListener, getActivity()));
        }

        return view;
    }

    //Setup UI method
    private void setupView() {
        // Set the adapter

        SharedPreference.getInstance(getActivity());
        appUserId = SharedPreference.getString("appUserId");

        //TODO
        SharedPreference.clearSharedPreference();

//
//            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
//                @Override
//                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//                    return true;
//                }
//
//                @Override
//                public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
//                    // Remove item from backing list here
//                    adapter.notifyDataSetChanged();
//                }
//            });
//
//            itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(ConversationModel item, int position);
    }

    private class FirebaseTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            //  mConversationModelList.clear();

            Query query = FirebaseDatabase.getInstance().getReference().child(FirebaseUtil.USER_CHAT_REFERENCE).orderByChild("userId").equalTo(appUserId);

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    for (DataSnapshot dS : dataSnapshot.getChildren()) {
                        conversationModel = new ConversationModel();

                        Map<String, Object> valuesMap = (HashMap<String, Object>) dS.getValue();

                        conversationModel.setPartnerName(valuesMap.get("chatName").toString());
                        conversationModel.setPartnerId(valuesMap.get("partnerId").toString());
                        conversationModel.setGroupId(valuesMap.get("groupId").toString());
                        conversationModel.setPhoto(valuesMap.get("chatPhoto").toString());
                        conversationModel.setUnreadMessagesCount(valuesMap.get("unreadMessages").toString());

                        mConversationModelList.add(conversationModel);

                    }
                    for (final ConversationModel model : mConversationModelList) {

                        Query userModelQuery = FirebaseDatabase.getInstance().getReference()
                                .child(FirebaseUtil.CHAT_GROUP_REFERENCE)
                                .orderByChild("id")
                                .equalTo(model.getGroupId());


                        userModelQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                for (DataSnapshot dS : dataSnapshot.getChildren()) {


                                    Map<String, Object> valuesMap = (HashMap<String, Object>) dS.getValue();

                                    model.setLastMessage(valuesMap.get("lastMessage").toString());
                                    model.setLastMessageDate((HashMap<String, Object>) valuesMap.get("lastMessageDate"));
                                }

                                RecyclerView recyclerView = (RecyclerView) view;

                                mAdapter = new MyConversationsItemRecyclerViewAdapter(mConversationModelList, mListener, getActivity());

                                recyclerView.setAdapter(mAdapter);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (view instanceof RecyclerView) {
                Context context = view.getContext();
                RecyclerView recyclerView = (RecyclerView) view;
                if (mColumnCount <= 1) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                } else {
                    recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
                }
            }
        }
    }
}
