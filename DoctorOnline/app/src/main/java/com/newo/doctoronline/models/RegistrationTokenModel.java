package com.newo.doctoronline.models;

/**
 * Created by elvinakhundzadeh on 6/27/17.
 */

public class RegistrationTokenModel {
    private String userId;
    private String token;

    public RegistrationTokenModel(String userId, String token) {
        this.userId = userId;
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
