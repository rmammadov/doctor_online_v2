package com.newo.doctoronline.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.newo.doctoronline.R;
import com.newo.doctoronline.models.UserModel;

import static com.newo.doctoronline.utils.FirebaseUtil.addUserFirebase;

public class CodeVerificationActivity extends AppCompatActivity {

    private AppCompatEditText etSmsCode;
    private Button bNext;

    private String mVerificationId;
    private String mResendToken;
    private String mPhoneNumber;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_verification);

        setupView();
    }

    private void setupView(){
        mContext = this;
        etSmsCode = (AppCompatEditText) findViewById(R.id.etxt_sms_code);
        bNext = (Button) findViewById(R.id.btn_verification_next);

        if(getIntent().getStringExtra("verificationId") != null && getIntent().getStringExtra("resendToken") != null){
            mVerificationId = getIntent().getStringExtra("verificationId");
            mResendToken = getIntent().getStringExtra("resendToken");

            Log.w("Verif. ID: ", mVerificationId);
            Log.w("Resend: ", mResendToken);
        }

        mPhoneNumber = getIntent().getStringExtra("phoneNumber");

        Log.w("userPhone: ", mPhoneNumber);


        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = etSmsCode.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    etSmsCode.setError("Cannot be empty.");
                    return;
                }
                verifyPhoneNumberWithCode(mVerificationId,code);
            }
        });
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
      //  TODO
         signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential){
        FirebaseAuth fAuth = FirebaseAuth.getInstance();

        fAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            Log.w("Sign in"," isSuccessful");

                            UserModel userModel = new UserModel();
                            userModel.setName("");
                            userModel.setPhoto_profile(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            userModel.setUserType("Patient");
                            userModel.setUserId(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            userModel.setPhone(mPhoneNumber);

                            addUserFirebase(userModel);

                            Intent intent = new Intent(mContext, SignUpProfileDetailsActivity.class);
                            intent.putExtra("appUserId", userModel.getUserId());
                            intent.putExtra("phoneNumber", userModel.getPhone());
                            startActivity(intent);
                            finish();

                        } else{
                            Toast.makeText(mContext,"Code is incorrect", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
