package com.newo.doctoronline.constants;

/**
 * Created by rmammadov on 8/20/16.
 */
public class Constants {

    /**
     * APP STATES
     * 0 - NEW IN APP
     * 1 - LOGGED OUT
     * 2 - LOGGED IN
     */

    // Splash screen duration
    public static int SPLASH_TIME_OUT = 3000;

    // Home pager scroll duration
    public static int SCROLL_TIME_OUT = 2000;

    // Shared preference tags
    public static String PREFERENCE_NAME_MAIN = "preference_doctoronline";
    public static String APP_STATE = "app_state";

    // App states
    public static int APP_STATE_DOWNLOADED = 0;
    public static int APP_STATE_LOGGED_OUT = 1;
    public static int APP_STATE_LOGGED_IN = 2;

    // Bugsee Id
    public static String BUGSEE_APP_ID = "8179120b-f960-4b62-9d18-896771594348";
}
