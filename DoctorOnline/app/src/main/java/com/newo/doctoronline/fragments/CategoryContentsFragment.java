package com.newo.doctoronline.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.newo.doctoronline.R;
import com.newo.doctoronline.adapters.CategoryContentsRecyclerViewAdapter;
import com.newo.doctoronline.models.UserModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class CategoryContentsFragment extends Fragment {

    public final static String TAG = "CategoryContentsFragment";
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private SwipeRefreshLayout mSwipeRefreshLayout;


    private List<UserModel> userList = new ArrayList<UserModel>() ;
    View view;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CategoryContentsFragment() {

    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CategoryContentsFragment newInstance(int columnCount) {
        CategoryContentsFragment fragment = new CategoryContentsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       view = inflater.inflate(R.layout.fragment_category_contents, container, false);


        mSwipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipe_category_contents);

        FirebaseTask firebaseTask = new FirebaseTask();
        firebaseTask.execute();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                FirebaseTask firebaseTask = new FirebaseTask();
                firebaseTask.execute();
            }
        });

//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                System.out.println("Swiped");
//
//                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("userModel");
//
//                ref.addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//
//                        userList.clear();
//
//                        System.out.println("There are " + dataSnapshot.getChildrenCount() + " users");
//
//                        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
//                            System.out.println("TestElvin:" + dsp.getValue(UserModel.class));
//                            userList.add(dsp.getValue(UserModel.class));
//                        }
//
//
//                        RecyclerView recyclerView = (RecyclerView) view;
//
//                       recyclerView.setAdapter(new CategoryContentsRecyclerViewAdapter(getActivity(), userList, mListener));
//                         //recyclerView.setAdapter(new CategoryContentsRecyclerViewAdapter(CategoryContent.ITEMS, mListener));
//
//                        mSwipeRefreshLayout.setRefreshing(false);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });
//            }
//        });

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new CategoryContentsRecyclerViewAdapter(getActivity(), userList, mListener));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(UserModel item, int postion);
    }

    private class FirebaseTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {

            Query query = FirebaseDatabase.getInstance().getReference().child("userModel").orderByChild("userType").equalTo("Undefined");

            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    userList.clear();

                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        userList.add(dsp.getValue(UserModel.class));
                    }

                    RecyclerView recyclerView = (RecyclerView) view;

                    recyclerView.setAdapter(new CategoryContentsRecyclerViewAdapter(getActivity(), userList, mListener));
                    //recyclerView.setAdapter(new CategoryContentsRecyclerViewAdapter(CategoryContent.ITEMS, mListener));

                    mSwipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mSwipeRefreshLayout.setRefreshing(false);
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);

                }
            });
        }
    }
}
