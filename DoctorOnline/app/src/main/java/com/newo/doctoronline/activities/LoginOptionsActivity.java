package com.newo.doctoronline.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.newo.doctoronline.R;
import com.newo.doctoronline.models.UserData;
import com.newo.doctoronline.models.UserModel;
import com.newo.doctoronline.utils.FirebaseUtil;
import com.newo.doctoronline.utils.SharedPreference;
import com.newo.doctoronline.utils.Utils;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

import static com.newo.doctoronline.utils.FirebaseUtil.addUserDataFirebase;
import static com.newo.doctoronline.utils.FirebaseUtil.addUserFirebase;

public class LoginOptionsActivity extends AppCompatActivity {

    private static final String TAG = "LoginOptionsActivity";
    private RelativeLayout rvBtnSignUp;
    private AppCompatTextView tvLogin;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private FirebaseAuth mAuth;
    private FirebaseStorage storage = FirebaseStorage.getInstance();

    private FirebaseUser mFirebaseUser;

    private File profilePhotoFile;

    private String facebookToken;
    private byte[] photoByteArray;

    private final String USER_MODEL_REFERENCE = "userModel";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginoptions);
        Log.w(TAG, "onCreate()");

        //Setup View
        setupView();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * Setup View components method
     */
    private void setupView() {
        //Initializations
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();
        //Setup Components
        rvBtnSignUp = (RelativeLayout) findViewById(R.id.rl_btn_signup);
        tvLogin = (AppCompatTextView) findViewById(R.id.tv_login_email);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        //Setup Ui interactions
        setupUiInteractions();
    }

    /**
     * Setup Ui interactions method
     */
    private void setupUiInteractions() {
        //On click methods
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // Handle access token with Firebase auth
                Log.d(TAG, "Logged in");
                facebookToken = loginResult.getAccessToken().getToken();
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                Log.d(TAG, "Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, "Error");
            }
        });

        rvBtnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignUpPage();
            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignInPage();
            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        Log.d(TAG, "MyToken: " + token.getToken());

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(LoginOptionsActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        } else if (task.isSuccessful()) {

                            addUserToFirebase();

                            openHomePage();
                        }
                    }
                });
    }

    private void addUserToFirebase() {

        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();

        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        if (mFirebaseUser == null) {
                Log.d(TAG, "FirebaseUser is null");
        } else {
            Log.d(TAG, "FirebaseUser is OK");
            Log.d(TAG, mFirebaseUser.getUid());
            Log.d(TAG, mFirebaseUser.getDisplayName());
            Log.d(TAG, mFirebaseUser.getEmail());
            Log.d(TAG, mFirebaseUser.getPhotoUrl().toString());
            Log.d(TAG, facebookToken);

            SharedPreference.saveString("appUserId", mFirebaseUser.getUid() );

            Query query = ref.child(USER_MODEL_REFERENCE).orderByChild("userId").equalTo( mFirebaseUser.getUid());

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(!dataSnapshot.exists()){

                       new FirebaseTask().execute();

                    //    addUserProfilePhoto(mFirebaseUser.getPhotoUrl().toString(), mFirebaseUser.getUid());

                        UserModel userModel = new UserModel();

                        userModel.setEmail(mFirebaseUser.getEmail());
                        userModel.setPhoto_profile(mFirebaseUser.getUid());
                        userModel.setName(mFirebaseUser.getDisplayName());
                     //   userModel.setLastLoginDate(Calendar.getInstance().getTime().getTime() + "");
                        userModel.setFacebookToken(facebookToken);
                        userModel.setUserType("Undefined");
                        userModel.setLoginType("Facebook");
                        userModel.setUserId(mFirebaseUser.getUid());

                        UserData userData = new UserData(userModel.getUserId());

                        addUserDataFirebase(userData);
                        addUserFirebase(userModel);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void addUserProfilePhoto(String url, String userId){
        InputStream is = null;

        System.out.println("Asynctask URL: " + url);
        System.out.println("Asynctask ID: " + userId);

        profilePhotoFile = new File(url);

        StorageReference storageRef = storage.getReferenceFromUrl(FirebaseUtil.URL_STORAGE_REFERENCE).child(FirebaseUtil.FOLDER_STORAGE_IMG);

        StorageReference storageReference = storageRef.child(userId);

        try {
            URL photoUrl = new URL(url);

            is = photoUrl.openStream();
            photoByteArray = Utils.readFully(is);

            System.out.println("Before uploading image: " + userId);

            UploadTask uploadTask = storageReference.putBytes(photoByteArray);
            System.out.println("After uploading image: " + userId);


            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
                    @SuppressWarnings("VisibleForTests")
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                 }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * open SignUp page
     */
    private void openSignUpPage() {
        Intent intentSignUp = new Intent(LoginOptionsActivity.this, SignUpActivity.class);
        startActivity(intentSignUp);
        finish();
    }

    /**
     * open SignIn page
     */
    private void openSignInPage() {
        Intent intentSignIn = new Intent(LoginOptionsActivity.this, SignInActivity.class);
        startActivity(intentSignIn);
        finish();
    }

    /**
     * open SignIn page
     */
    private void openHomePage() {
        Intent intentHome = new Intent(this, HomeActivity.class);
        startActivity(intentHome);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private class FirebaseTask extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... params) {
            addUserProfilePhoto(mFirebaseUser.getPhotoUrl().toString(), mFirebaseUser.getUid());

            return null;
        }
    }
}
