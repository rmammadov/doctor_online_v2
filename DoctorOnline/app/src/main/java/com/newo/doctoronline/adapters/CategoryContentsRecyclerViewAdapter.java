package com.newo.doctoronline.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.newo.doctoronline.R;
import com.newo.doctoronline.fragments.CategoryContentsFragment;
import com.newo.doctoronline.models.UserModel;
import com.newo.doctoronline.utils.CircleImageUtil;
import com.newo.doctoronline.utils.FirebaseUtil;

import java.util.List;

import static com.newo.doctoronline.utils.FirebaseUtil.storage;

/**
 * Created by elvinakhundzadeh on 6/6/17.
 */

public class CategoryContentsRecyclerViewAdapter extends RecyclerView.Adapter<CategoryContentsRecyclerViewAdapter.ViewHolder>  {

    private final List<UserModel> mValues;
    private final CategoryContentsFragment.OnListFragmentInteractionListener mListener;
    private final Context mContext;

    public CategoryContentsRecyclerViewAdapter(Context context, List<UserModel> items, CategoryContentsFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_category_contents_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getName());
        holder.mContentView.setText("");

        StorageReference storageRef = storage.getReferenceFromUrl(FirebaseUtil.URL_STORAGE_REFERENCE).child(FirebaseUtil.FOLDER_STORAGE_IMG);

        storageRef.child(mValues.get(position).getUserId()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(mContext.getApplicationContext())
                        .load(uri)
                        .transform(new CircleImageUtil(mContext.getApplicationContext()))
                        .into(holder.mImageView);
            }
        });

       /* Glide.with(mContext)
                .load(mValues.get(position).getPhoto_profile())
                .transform(new CircleImageUtil(mContext))
                .into(holder.mImageView);*/

        //TODO
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final ImageView mImageView;
        public UserModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.tv_category_contents_list_item_name);
            mContentView = (TextView) view.findViewById(R.id.tv_category_contents_list_item_description);
            mImageView = (ImageView) view.findViewById(R.id.image_rounded_list_item_category_contents);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
