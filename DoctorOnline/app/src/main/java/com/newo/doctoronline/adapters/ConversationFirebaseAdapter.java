package com.newo.doctoronline.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.newo.doctoronline.R;
import com.newo.doctoronline.utils.CircleImageUtil;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by elvinakhundzadeh on 6/14/17.
 */

public class ConversationFirebaseAdapter extends RecyclerView.Adapter<ConversationFirebaseAdapter.ConversationViewHolder>{


    private static final int RIGHT_MSG = 0;
    private static final int LEFT_MSG = 1;
    private static final int RIGHT_MSG_IMG = 2;
    private static final int LEFT_MSG_IMG = 3;

    public ConversationFirebaseAdapter() {
    }

    @Override
    public ConversationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == RIGHT_MSG){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right,parent,false);
            return new ConversationFirebaseAdapter.ConversationViewHolder(view);
        }else if (viewType == LEFT_MSG){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left,parent,false);
            return new ConversationFirebaseAdapter.ConversationViewHolder(view);
        }else if (viewType == RIGHT_MSG_IMG){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right_img,parent,false);
            return new ConversationFirebaseAdapter.ConversationViewHolder(view);
        }else{
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left_img,parent,false);
            return new ConversationFirebaseAdapter.ConversationViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ConversationViewHolder holder, int position) {

        holder.setIvUser("User");
        holder.setTxtMessage("Message");
        holder.setTvTimestamp("Timestamp");
        holder.tvIsLocation(View.GONE);

       /* holder.setIvUser(model.getUserModel().getPhoto_profile());
        holder.setTxtMessage(model.getMessage());
        holder.setTvTimestamp(model.getTimeStamp());
        holder.tvIsLocation(View.GONE);
        if (model.getFile() != null){
            holder.tvIsLocation(View.GONE);
            holder.setIvChatPhoto(model.getFile().getUrl_file());
        }else if(model.getMapModel() != null){
            holder.setIvChatPhoto(Utils.local(model.getMapModel().getLatitude(),model.getMapModel().getLongitude()));
            holder.tvIsLocation(View.VISIBLE);
        }*/
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ConversationViewHolder extends RecyclerView.ViewHolder{

        TextView tvTimestamp,tvLocation;
        EmojiconTextView txtMessage;
        ImageView ivUser,ivChatPhoto;

        public ConversationViewHolder(View itemView) {
            super(itemView);

            tvTimestamp = (TextView)itemView.findViewById(R.id.timestamp);
            txtMessage = (EmojiconTextView)itemView.findViewById(R.id.txtMessage);
            tvLocation = (TextView)itemView.findViewById(R.id.tvLocation);
            ivChatPhoto = (ImageView)itemView.findViewById(R.id.img_chat);
            ivUser = (ImageView)itemView.findViewById(R.id.ivUserChat);
        }


        public void setTxtMessage(String message){
            if (txtMessage == null)return;
            txtMessage.setText(message);
        }

        public void setIvUser(String urlPhotoUser){
            if (ivUser == null)return;
            Glide.with(ivUser.getContext()).load(urlPhotoUser).centerCrop().transform(new CircleImageUtil(ivUser.getContext())).override(40,40).into(ivUser);
        }

        public void setTvTimestamp(String timestamp){
            if (tvTimestamp == null)return;
            tvTimestamp.setText(DateUtils.getRelativeTimeSpanString(Long.parseLong(timestamp),System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        }

        public void setIvChatPhoto(String url){
            if (ivChatPhoto == null)return;
            Glide.with(ivChatPhoto.getContext()).load(url)
                    .override(100, 100)
                    .fitCenter()
                    .into(ivChatPhoto);
            //   ivChatPhoto.setOnClickListener(this);
        }

        public void tvIsLocation(int visible){
            if (tvLocation == null)return;
            tvLocation.setVisibility(visible);
        }
    }

}
