package com.newo.doctoronline.models;

/**
 * Created by elvinakhundzadeh on 6/19/17.
 */

public class LocationModel {
    private double lat;
    private double lng;

    public LocationModel(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public LocationModel() {
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
