package com.newo.doctoronline.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.newo.doctoronline.R;
import com.newo.doctoronline.models.UserModel;

import java.util.concurrent.TimeUnit;

import pl.tajchert.nammu.Nammu;

import static com.newo.doctoronline.utils.FirebaseUtil.USER_MODEL_REFERENCE;
import static com.newo.doctoronline.utils.FirebaseUtil.addUserFirebase;
import static com.newo.doctoronline.utils.FirebaseUtil.firebaseDatabaseReference;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "SignUpActivity";

    private RelativeLayout rlRegistrationMethod;
    private AppCompatButton btnNext;
    private NavigationTabStrip ntsRegisterOptions;
    private LinearLayoutCompat llPhone;
    private TextInputLayout inputLayoutEmail;
    private NavigationTabStrip navigationTabStrip;
    private TextInputLayout inputLayoutPhone;

    private int userCheck = 9;

    private FirebaseAuth mAuth;

    private ProgressDialog mProgressDialog;

    private Context mContext;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private boolean mVerificationInProgress;
    private String mVerificationId;
    private String mPhoneNumber;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //Call setupView method
        setupView();

        //Setup interactions of components
        setupUiInteractions();
    }

    /**
     * Setup View components method
     */
    private void setupView() {
        //Setup Components
        ntsRegisterOptions = (NavigationTabStrip) findViewById(R.id.nts_register_options);
        llPhone = (LinearLayoutCompat) findViewById(R.id.ll_phone);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        rlRegistrationMethod = (RelativeLayout) findViewById(R.id.rl_registration_method);
        btnNext = (AppCompatButton) findViewById(R.id.btn_next);
        ntsRegisterOptions = (NavigationTabStrip) findViewById(R.id.nts_register_options);
        inputLayoutPhone = (TextInputLayout) findViewById(R.id.input_layout_phone);

        mProgressDialog = new ProgressDialog(this);

        mContext = SignUpActivity.this;
        mAuth = FirebaseAuth.getInstance();


        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Log.d(TAG, "onVerificationCompleted:" + phoneAuthCredential);

                mVerificationInProgress = false;

                FirebaseAuth fAuth = FirebaseAuth.getInstance();

                fAuth.signInWithCredential(phoneAuthCredential)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    Log.w("Sign in", " isSuccessful");

                                    UserModel userModel = new UserModel();
                                    userModel.setName("");
                                    userModel.setPhoto_profile(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                    userModel.setUserType("Patient");
                                    userModel.setUserId(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                    userModel.setPhone(mPhoneNumber);

                                    addUserFirebase(userModel);

                                    Intent intent = new Intent(mContext, SignUpProfileDetailsActivity.class);
                                    intent.putExtra("appUserId", userModel.getUserId());
                                    intent.putExtra("phoneNumber", userModel.getPhone());
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);

                mVerificationInProgress = false;

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    inputLayoutPhone.setError("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                Log.d(TAG, "onCodeSent:" + s);

                mVerificationId = s;
                mResendToken = forceResendingToken;

                Intent intent = new Intent(SignUpActivity.this, CodeVerificationActivity.class);

                Log.w(TAG, "Verification code sent");
                Log.w(TAG, "VerificationID: " + s);
                Log.w(TAG, "resendToken: " + forceResendingToken.toString());

                intent.putExtra("verificationId", s);
                intent.putExtra("resendToken", forceResendingToken.toString());
                intent.putExtra("phoneNumber", inputLayoutPhone.getEditText().getText().toString());

                startActivityForResult(intent, 78);

                //TODO
                //   updateUI(STATE_CODE_SENT);
            }
        };

    }

    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        Log.w(TAG, "Verification started");

        mVerificationInProgress = true;
    }

    private void setupUiInteractions() {
        //On click methods

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPhoneNumber = inputLayoutPhone.getEditText().getText().toString();

                Query query = firebaseDatabaseReference.child(USER_MODEL_REFERENCE).orderByChild("phone").equalTo(mPhoneNumber);

                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Log.w("User:", " exists");
                            Toast.makeText(mContext, "This number is already registered...", Toast.LENGTH_LONG).show();
                        } else {
                            Log.w("User:", " DOESN'T exist");

                            startPhoneNumberVerification(mPhoneNumber);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
//                new FirebaseTask().execute(mPhoneNumber);

            }
        });

        ntsRegisterOptions.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {
                if (index == 0) {
                    llPhone.setVisibility(View.VISIBLE);
                    inputLayoutEmail.setVisibility(View.GONE);
                } else {
                    Intent intent = new Intent(SignUpActivity.this, SignUpProfileDetailsActivity.class);
                    startActivity(intent);
                    //llPhone.setVisibility(View.GONE);
                    //inputLayoutEmail.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onEndTabSelected(String title, int index) {

            }
        });
        ntsRegisterOptions.setTabIndex(0, true);
    }

    /**
     * Ask for permission if required method
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Activity result of easy image
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
