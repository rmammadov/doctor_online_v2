package com.newo.doctoronline.models;

/**
 * Created by elvinakhundzadeh on 6/10/17.
 */

public class TestModel {
    private String partnerId;
    private String groupId;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
