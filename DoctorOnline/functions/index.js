// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });



// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
var receiverId;
var msg;
var token;
var sender;


exports.sendFollowerNotification = functions.database.ref('/chatmodel/{chatId}').onWrite(event => {
   const followerUid = event.params.chatId;
   receiverId = event.data.val().receiverId;
   msg = event.data.val().message;
   sender = event.data.val().senderId;

    console.log('We have a new message :', followerUid);
    console.log('Message receiver is :', receiverId);
    console.log('Message  is :', msg);


    return loadUser().then(user => {

        console.log('user b4 token :', user);

        console.log('ReceiverToken is :', token);

        let payload = {
            notification: {
                title: sender,
                body: msg,
                sound: "default",
                badge: "1",
                click_action: "OPEN_CONVERSATION_ACTIVITY"
            },
            data: {
               partnerId: sender
             }
        }

        return admin.messaging().sendToDevice(token, payload);

    });
});

function loadUser(){

    let dbRef = admin.database().ref('/regToken');
    let defer = new Promise((resolve, reject)=>{
        dbRef.orderByChild('userId').equalTo(receiverId).once('value', (snap)=>{


            let data = snap.val();
            var key;
            for(var field in data){
                key = field;
                break
            }

            var partnerToken = data[key]["token"]
            token = partnerToken;

            console.log('Partners token is :', partnerToken);
            console.log('Data is :', data);

            let user = receiverId;

            console.log('User is :', user);

            resolve(user);

        }, (err) =>{
            reject(err);
        });
    });

    return defer;

}
